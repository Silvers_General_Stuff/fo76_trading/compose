import {
    Entity,
    Column,
    ObjectIdColumn,
    CreateDateColumn,
    OneToOne,
    UpdateDateColumn
} from "typeorm";
import { JoinColumn, Index } from "typeorm";

// each entiity is a table

@Entity()
export class User_Steam {
    @ObjectIdColumn()
    _id?

    @Index({ unique: true })
    @Column()
    id: string

    @CreateDateColumn()
    date_added?

    @UpdateDateColumn()
    date_updated?

    @Column()
    provider: string

    @Column()
    displayName: string
}

@Entity()
export class User {
    @ObjectIdColumn()
    _id?

    // from steam
    @Index({ unique: true })
    @Column()
    id: string

    // from steam
    @Column()
    displayName: string

    // first added
    @CreateDateColumn()
    date_added?

    @UpdateDateColumn()
    date_updated?

    // Intergrations

    // steam
    @OneToOne(() => User_Steam, {nullable: true})
    @JoinColumn()
    steam: User_Steam

    // migrating to local storage
    @Column({default: false})
    migrated: boolean;


    @Column({default: []})
    characters: string[];
}

abstract class Season_Base {
    @ObjectIdColumn()
    _id?

    @Index({ unique: true })
    @Column()
    season: number;

    @Column()
    name: string;

    @Column({type: "timestamptz"})
    start: string;

    @Column({type: "timestamptz"})
    end: string;
}

@Entity()
export class Season extends Season_Base {
    // the name of the season
    //name: string;

    @Column()
    base: number;

    @Column()
    extra: number;

    // score to lvl up = base + (extra * {level - 1})

    // this is the final date if it gets extended
    @Column({type: "timestamptz"})
    end_final: string;

    // these relate to the score available
    @Column()
    score_daily: number;
    @Column()
    score_daily_nw: number;
    @Column()
    score_weekly: number;

    // this is the amount of xp needed to get 1 score
    @Column({type: "float"})
    ratio: number;

    // cost per level in atoms
    @Column()
    atoms: number;


    // this is the max score lvl for teh board
    @Column()
    score_level_board: number;

    // maximum score lvl you can get, same as above except if its unlimited, then 1000
    @Column()
    score_level_max: number;
}

@Entity()
export class Season_Events extends Season_Base {
    // the name of the event during a season
    //name: string;

}

// move these to teh entities later
export enum Season_Rewards_Rarity {
    Common = "Common",
    Rare = "Rare",
    Legendary = "Legendary"
}
export enum Season_Rewards_Type {
    Icon = "Icon",
    Consumable = "Consumable",
    CAMP = "CAMP",
    Frame = "Frame",
    Currency = "Currency",
    Skin = "Skin",
    Flair = "Flair",
    Pose = "Pose",
    Outfit = "Outfit",
    PA = "Power Armor",
    Stein = "Stein",
    Emote = "Emote",
    Ally = "Ally",
}

@Entity()
//@Index(["season", "level"])
//@Unique(["season", "level", "first"])
export class Season_Rewards {
    @ObjectIdColumn()
    _id?

    @Index({ unique: true })
    @Column()
    id?: string;

    @Column()
    first: boolean;

    @Column()
    @Index()
    season: number;

    @Column()
    level: number;

    @Column()
    name: string;

    @Column()
    type: Season_Rewards_Type;

    @Column()
    quantity: number;

    @Column()
    rarity: Season_Rewards_Rarity;
}

// this is the userdata, updated for each level
@Entity()
export class Season_User {
    @ObjectIdColumn()
    _id?

    // boilerplate stuff first
    @Index({ unique: true })
    @Column()
    id?: string;

    @Index()
    @Column()
    season: number;

    @Index()
    @Column()
    user: string

    @Column()
    date_updated: string

    @Column()
    level: number;
}

@Entity()
export class Season_User_Config {
    @ObjectIdColumn()
    _id?

    // misc data for storage
    @Index({ unique: true })
    @Column()
    id?: string;

    @Column()
    season: number;

    @Column()
    user: string;

    @Column()
    date_start: string;
    // xp: number;

    // character levels (if playing one character only)
    @Column()
    level_start:number;

    @Column()
    level_end:number;
}


export enum TaskList_Entry_Reset_Type {
    Offset = "offset",
    Day = "day",
    Hour = "hour",
}

export interface TaskList_Entry_Reset {
    // offset/day/time
    type: TaskList_Entry_Reset_Type;
    value: number;
    value_secondary?: number;
}

@Entity()
@Index(["user", "character"])
export class TaskList_Entry {
    @ObjectIdColumn()
    _id?

    @Index({ unique: true })
    @Column()
    id: string;

    @Column()
    user: string;

    @Column()
    character: string;

    @Column()
    order: number;

    @Column()
    name: string;

    @Column()
    date: string;

    @Column()
    offset: TaskList_Entry_Reset;
}

// this is an array of all the entities
export const entities = [
    User,
    User_Steam,

    Season,
    Season_Events,
    Season_Rewards,
    Season_User,
    Season_User_Config,
    TaskList_Entry
];