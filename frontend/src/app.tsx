import React, {Component} from "react";
import {
    HashRouter,
    Switch,
    Route
} from "react-router-dom";
import cookie from 'react-cookies'

import {Sync} from "./content/sync"
import {Home} from "./content/home"
import {get_data, post_data, Seasons} from "./content/seasons"
import { TaskList } from "./content/task_list"
import {
    HeaderContainer,
    Header,
    SkipToContent,
    HeaderMenuButton,
    HeaderName,
    HeaderNavigation,
    HeaderMenu,
    HeaderMenuItem,
    SideNav,
    SideNavItems,
    HeaderSideNavItems,
    Content
} from 'carbon-components-react';

// interfaces
import { Data_User_Account_Data, Season, Season_Config, Season_User } from "./config/interfaces"
import {leftDB} from "./config/remoteStorage"

interface App_State {
    user?: Data_User_Account_Data;
    backend: string;
}
export class App extends Component <any, App_State>{
    constructor(props: any) {
        super(props);
        let backend
        let { REACT_APP_BACKEND_ADDRESS } = process.env
        if(REACT_APP_BACKEND_ADDRESS){
            backend = REACT_APP_BACKEND_ADDRESS
        }
        this.state = {
            user: undefined,
            backend: backend || "https://api.fo76.ie",
        };
    }

    // if the jwt exists and user is undefined then fetch it
    getUser = async() =>{
        let url = `${this.state.backend}/account`
        let result: Response = await fetch(url, {
            method: 'GET',
            credentials: 'include'
        })
            .catch((err)=> {console.log(err);return err})

        if(result.status === 200){
            let result_tmp: {
                status: string,
                success: Data_User_Account_Data
            } = await result.json()

            if (result_tmp.status === "success"){
                let account = result_tmp.success;
                this.setState({user: account})

                if(!account.migrated){
                    // save teh users season data
                    await this.convert_user_season(account)
                }
            }
        }
    }

    convert_user_season = async(account: Data_User_Account_Data) =>{
        let {backend} = this.state;

        const remoteStorage = leftDB();

        // get season data
        let seasons = await get_data<Season>(`${backend}/season`);

        let tmp: {
            config: Season_Config[]
            user: Season_User[]
            characters: string[];
        } = {
            config: [],
            user: [],
            characters: [],
        }

        // then for each season save teh user data
        for(let i=0;i<seasons.length;i++){
            let season = seasons[i];

            // config first
            let config_base = await remoteStorage["fo76_ie/config"].get<Season_Config>(`season_${season.season}`);
            if(config_base.length >0){
                let config = config_base[0];
                let temp: Season_Config = {
                    id: `${account.id}_${season.season}`,

                    user: account.id,
                    season: season.season,

                    date_start: config.date_start || "",
                    level_start: config.level_start || 2,
                    level_end: config.level_end || 2,
                }
                tmp.config.push(temp);
            }

            // season data
            let scores = await remoteStorage["fo76_ie/season_user"].get(season.season);
            for(let score of scores){
                let temp: Season_User = {
                    id: `${account.id}_${season.season}_${score.level}`,

                    user: account.id,
                    season: season.season,

                    date_updated: score.date_updated || "",
                    level: score.level || 2,
                }
                tmp.user.push(temp);
            }


        }

        tmp.characters = await remoteStorage["fo76_ie/task_list"].characters.get("characters");

        await post_data("POST", `${this.state.backend}/account/migrate`, tmp)
    }


    componentDidMount = async () => {
        // then if the user is logged in get teh user data
        if(cookie.load("loggedIn") && typeof this.state.user === "undefined"){
            await this.getUser()
        }
    }

    logout = async () =>{
        let url = `${this.state.backend}/account/logout`
        let result: Response = await fetch(url, {
            method: 'GET',
            credentials: 'include'
        })
            .catch((err)=> {console.log(err);return err})

        if(result.status === 200){
            this.setState({user: undefined})
        }
    }


    render() {
        let {user, backend} = this.state

        let nav_links = [
            <HeaderMenuItem href="#/" key={`nav_links-1`}>Home</HeaderMenuItem>,
        ];

        let routes;

        // login/name first
        if(!user){
            nav_links = [
                <HeaderMenuItem href="#/link" key={`nav_links-0`}>Sign in</HeaderMenuItem>,
                <HeaderMenuItem href="#/" key={`nav_links-1`}>Home</HeaderMenuItem>,
            ];

            routes = [
                <Route key={`route-0`} path="/link" ><Sync backend={backend} /></Route>,
                <Route key={`route-1`} path="/" exact><Home /></Route>,
            ]
        }else{
            nav_links = [
                <HeaderMenu aria-label="Username" menuLinkName={user.displayName} key={`nav_links-0`}>
                    <HeaderMenuItem onClick={async() => {await this.logout()}} key={`nav_links-0-0`} >Logout</HeaderMenuItem>
                </HeaderMenu>,
                <HeaderMenuItem href="#/" key={`nav_links-1`}>Home</HeaderMenuItem>,
                <HeaderMenuItem href="#/season" key={`nav_links-2`}>Season</HeaderMenuItem>,
                <HeaderMenuItem href="#/task_list" key={`nav_links-3`}>Task List</HeaderMenuItem>,
            ];

            routes = [
                <Route key={`route-1`} path="/" exact><Home /></Route>,
                <Route key={`route-2`} path="/season" ><Seasons backend={backend} /></Route>,
                <Route key={`route-2`} path="/task_list" ><TaskList backend={backend} user={user} /></Route>,
            ]
        }

        let navbar = <HeaderContainer
            render={({ isSideNavExpanded, onClickSideNavExpand }) => (
                <Header aria-label="Fo76.ie">
                    <SkipToContent />
                    <HeaderMenuButton
                        aria-label="Open menu"
                        onClick={onClickSideNavExpand}
                        isActive={isSideNavExpanded}
                    />
                    <HeaderName href="#" prefix="">Fo76.ie</HeaderName>
                    <HeaderNavigation aria-label="Fo76.ie">
                        {nav_links}
                    </HeaderNavigation>
                    <SideNav
                        aria-label="Side navigation"
                        expanded={isSideNavExpanded}
                        isPersistent={false}>
                        <SideNavItems>
                            <HeaderSideNavItems>
                                {nav_links}
                            </HeaderSideNavItems>
                        </SideNavItems>
                    </SideNav>
                </Header>
            )}
        />

        return <HashRouter>
            <div>
                {navbar}
                <Content>
                    <Switch>
                        {routes}
                    </Switch>
                </Content>
            </div>
        </HashRouter>
    }
}