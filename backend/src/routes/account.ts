import { Ctx } from "../index"
import {Season_User_Config, Season_User, User} from "../config/db_entities";
import { PG_Manager } from "../config/db";

export class Result_Object<T> {
    status: string;
    error?: string;
    success?: T;
}

export default (ctx: Ctx) => {
    const {passport, pg, server} = ctx

    // this is for getting the users own account info
    server.get('/account',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        let result = await account_get_own(pg, req.user as User)
        res.send(result)
        next()
    })

    // this is for updating the users own account info
    server.post('/account',passport.authenticate('jwt', { session: false }), async (req, res, next) => {
        if(typeof req.body === "undefined"){
             console.log({result: "error", error: "no body"})
        }
        let result = await account_update(pg, req.user as User, req.body)
        res.send(result)
        next()
    })

    server.get('/account/logout',(req, res) => {
        return res
            .status(200)
            .clearCookie('jwt', {
                domain:"fo76.ie",
                httpOnly: true,
                secure: true,
            })
            // this is to let the browser know that the user is logged in, no other purporse
            .clearCookie('loggedIn', {
                domain:"fo76.ie",
                httpOnly: false,
                secure: true,
            })
            .send();
            //.redirect(`${config.frontend}#/login`)
    });

    // temporty endpoint
    server.post('/account/migrate', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        let result = await migrate(pg, req.user as User, req.body);
        res.send(result);
    })
}

async function account_get_own (pg: PG_Manager, user:User): Promise<Result_Object<User>>{
    let results = await pg.get_items(User,{ id: user.id} );

    if(results.length === 0){
        return {status: "error", error: "not found"}
    }else{
        return {status: "success", success: results[0]}
    }
}

async function account_update(pg: PG_Manager, user:User, newData: Partial<User>): Promise<Result_Object<string>>{
    if(!newData){return {status: "error", error:"Could not update account, no data"}}

    return await pg.update_bulk(User, [newData])
        .then(()=>{return {status: "success", success:"Account updated"}})
        .catch(err => {console.log(err); return {status: "error", error:"Could not update account"}})
}

interface Migrate {
    config: Season_User_Config[]
    user: Season_User[]
    characters: string[]
}
async function migrate (pg: PG_Manager, user:User, newData: Migrate): Promise<Result_Object<string>>{
    return await Promise.all([
        await pg.update_bulk(Season_User_Config, newData.config),
        await pg.update_bulk(Season_User, newData.user),
        await pg.update_bulk(User, [{id: user.id, characters: newData.characters, migrated: true}])
    ])
        .then(()=> {return {status: "success", success: "added"}})
        .catch(err => {console.log(err); return {status: "error", error: "failed to add"}})
}