

// user
export interface User extends Object {
    id: string;
    displayName: string
    photos: object[]
    first_added: Date
    trades_completed:number
}

// what gets sent to the server for each legendary


export interface Data_User_Account_Data {
    // basic data
    id: string
    date_added: string
    displayName: string

    migrated: boolean;
    characters: string[];
}

export interface ExtendedWindow extends Window {
    _env_?: any;
}

// from backend

interface Season_Base {
    season: number;
    name: string;
    start: string;
    end: string;
}

export interface Season extends Season_Base {
    // the name of the season
    //name: string;
    base: number;
    extra: number;

    // score to lvl up = base + (extra * {level - 1})

    // this is the amount of xp needed to get 1 score

    ratio: number;

    // cost per level in atoms

    atoms: number;

    // the final date, after it has been extended
    end_final: string;

    // these relate to the score available
    score_daily: number;
    score_daily_nw: number;
    score_weekly: number;

    // this is the max score lvl for teh board
    score_level_board: number;

    // maximum score lvl you can get, same as above except if its unlimited, then 1000
    score_level_max: number;
}

export interface Season_Events extends Season_Base {
    // the name of the event during a season
    //name: string;
}

// duplicated from the backend
export enum Season_Rewards_Rarity {
    Common = "Common",
    Rare = "Rare",
    Legendary = "Legendary"
}
export enum Season_Rewards_Type {
    Icon = "Icon",
    Consumable = "Consumable",
    CAMP = "CAMP",
    Frame = "Frame",
    Currency = "Currency",
    Skin = "Skin",
    Flair = "Flair",
    Pose = "Pose",
    Outfit = "Outfit",
    PA = "Power Armor",
    Stein = "Stein",
    Emote = "Emote",
    Ally = "Ally",
}
export interface Season_Rewards {
    id: string;
    first: boolean;
    season: number;
    level: number;
    name: string;
    type: Season_Rewards_Type;
    quantity: number;
    rarity: Season_Rewards_Rarity;
}

// this is the userdata, updated for each level
export interface Season_User_Old {
    id?: string;

    season: number;

    user: string

    date_updated: string

    level: number;
}

export interface Season_User {
    id?: string;
    season: number;
    user: string;
    date_updated: string
    level: number;
}

// this is on a per season config
export interface Season_Config {
    id?: string;
    season: number;
    user: string;

    date_start: string;
    level_start:number;
    level_end:number;
}

export interface Season_Config_Global {
    page: string;
    idiot_savant: boolean;
}

export enum TaskList_Entry_Reset_Type {
    Offset = "offset",
    Day = "day",
    Hour = "hour",
}

export interface TaskList_Entry_Reset {
    // offset/day/time
    type: TaskList_Entry_Reset_Type;
    value: number;
    value_secondary?: number;
}
export interface TaskList_Entry {
    id: string;
    user: string;
    character: string;
    order: number;
    name: string;
    date: string;
    offset: TaskList_Entry_Reset;
}