
// for Ctx
import express, {Express} from "express";
import { PassportStatic } from "passport"
import { PG_Manager } from "./config/db"

import routes from "./config/routes"
import config from "./config/config"
import {passportSetup} from "./config/passport"
import session from "express-session"
import cookieParser  from "cookie-parser"
import bodyParser from "body-parser"
import cors from "cors"

main().catch(err => console.log(err));
async function main(){
    const server = express();

    let pg = new PG_Manager();
    await pg.up();
    // seed the raw data
    await pg.seed();

    let passport = passportSetup(pg);

    server.use(cors({
        origin:[
            // testing env
            "http://localhost:3000",

            // testing env
            "https://dev.fo76.ie",

            // public test
            "https://beta.fo76.ie",

            // production
            "https://fo76.ie", "https://www.fo76.ie",
        ],
        credentials: true
    }))

    server.use(session({ secret:config.sessionSecret }));

    // get cookies
    server.use(cookieParser());

    // get the body
    // parse application/x-www-form-urlencoded
    server.use(bodyParser.urlencoded({ extended: false }))
    // parse application/json
    server.use(bodyParser.json())

    server.use(passport.initialize());
    server.use(passport.session());

    // start the Express server
    server.listen( config.port, async () => {
        routes({pg: pg, server: server, passport:passport});
        console.log( `server started at http://localhost:${ config.port }` );
    } );
}

export class Ctx {
    pg: PG_Manager
    server: Express
    passport: PassportStatic
}