import React, {  Component } from "react";

interface Login_State {
    backend: string;
}
interface Login_Props {
    backend: string;
}
export class Sync extends Component <Login_Props, Login_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            backend: this.props.backend
        }
    }

    render() {
        let { backend } = this.state;
        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row">
                <div className="bx--col">
                    <h3>
                        Sign in with Steam
                    </h3>
                    <p>
                        Steam is used as an Oauth provider (like Google, Facebook, Reddit or Discord).
                        <br />
                        No steam user info of note is stored
                    </p>
                    <br />
                    <a href={`${backend}/auth/steam`}><img src={"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/steamworks_docs/english/sits_large_noborder.png"} alt="Sync with Steam"/></a>
                </div>
            </div>
        </div>
    }
}