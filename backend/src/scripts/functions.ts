

export function compare( a:object, b:object, field:string ) {
    if ( a[field] < b[field] ){
        return -1;
    }
    if ( a[field] > b[field] ){
        return 1;
    }
    return 0;
}