import { Ctx } from "../index"
import {User, Season, Season_Events, Season_Rewards, Season_User, Season_User_Config} from "../config/db_entities";
import { PG_Manager } from "../config/db";
import { Result_Object } from "./account";

export default (ctx: Ctx) => {
    const {passport, pg, server} = ctx

    // season base data
    server.get('/season', async (req, res) => {

        let result = await pg.get_items(Season, {})
        res.send(result);
    })

    // events for the season
    server.get('/season/events/:season', async (req, res) => {

        let result = await pg.get_items(Season_Events, {season: parseInt(req.params.season, 10)})
        res.send(result);
    })

    // season rewards
    server.get('/season/rewards/:season', async (req, res) => {
        let result = await pg.get_items(Season_Rewards, {season: parseInt(req.params.season, 10)})
        res.send(result);
    })

    // authenticated
    // gets the users records for a season
    server.get('/season/user/:season', passport.authenticate('jwt', { session: false }), async (req, res) => {
        let user = req.user as User;
        let result = await pg.get_items(Season_User, {season: parseInt(req.params.season, 10), user: user.id})
        res.send(result);
    })

    // sets the users records for a season
    server.post('/season/user/:season', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        let result = await update_season_user_add(pg, req.user as User, parseInt(req.params.season, 10), req.body);
        res.send(result);
    })

    server.delete('/season/user/:season', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        let result = await update_season_user_remove(pg, req.user as User, parseInt(req.params.season, 10), req.body);
        res.send(result);
    })


    server.get('/season/config/:season', passport.authenticate('jwt', { session: false }), async (req, res) => {
        let user = req.user as User;
        let result = await pg.get_items(Season_User_Config, {season: parseInt(req.params.season, 10), user: user.id})
        res.send(result);
    })


    server.post('/season/config/:season', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        let result = await update_season_user_config(pg, req.user as User, parseInt(req.params.season, 10), req.body);
        res.send(result);
    })
}

async function update_season_user_add (pg: PG_Manager, user:User, season: number, newData: Partial<Season_User>): Promise<Result_Object<string>>{
    // check if level is provided
    if (!newData.level){
        return {status: "error", error: "no level"}
    }

    // check if the season exists
    let seasons = await pg.get_items(Season, {season: season});
    if(seasons.length === 0){
        return {status: "error", error: "no season"}
    }
    // check if its within the date range + 3 days at the end
    let season_data = seasons[0];

    let now = new Date().toISOString();

    if(now < season_data.start){
        return {status: "error", error: "season not started"}
    }

    let three_days = new Date(season_data.end_final);
    three_days.setDate(three_days.getDate() + 3);

    if(now > three_days.toISOString()){
        return {status: "error", error: "season has ended"}
    }

    let tmp:Season_User = {
        id:`${user.id}_${season}_${newData.level}`,
        user: user.id,
        level: newData.level,
        season: season,
        date_updated: new Date().toISOString()
    }

    return await pg.update_bulk(Season_User, [tmp])
        .then(()=> {return {status: "success", success: "added"}})
        .catch(err => {console.log(err); return {status: "error", error: "failed to add"}})
}

async function update_season_user_remove (pg: PG_Manager, user:User, season: number, newData: any): Promise<Result_Object<string>>{
    // check if level is provided
    if (!newData.level){
        return {status: "error", error: "no level"}
    }

    // check if the season exists
    let seasons = await pg.get_items(Season, {season: season});
    if(seasons.length === 0){
        return {status: "error", error: "no season"}
    }

    let tmp:Partial<Season_User> = {
        // +1 as its referring to the last one it was on
        level: parseInt(newData.level, 10) + 1,
        season: season,
        user: user.id
    }

    return await pg.delete<Season_User>(Season_User, tmp)
        .then(()=> {return {status: "success", success: "added"}})
        .catch(err => {console.log(err); return {status: "error", error: "failed to add"}})
}

async function update_season_user_config (pg: PG_Manager, user:User, season: number, newData: Partial<Season_User_Config>){

    let seasons = await pg.get_items(Season, {season: season});
    if(seasons.length === 0){
        return {status: "error", error: "no season"}
    }
    // check if its within the date range + 3 days at the end
    let season_data = seasons[0];

    let now = new Date().toISOString();

    if(now < season_data.start){
        return {status: "error", error: "season not started"}
    }

    let three_days = new Date(season_data.end_final);
    three_days.setDate(three_days.getDate() + 3);

    if(now > three_days.toISOString()){
        return {status: "error", error: "season has ended"}
    }

    let tmp:Partial<Season_User_Config> = {
        user: user.id,
        season: season,

        date_start: newData.date_start || "",
        level_start: newData.level_start || 1,
        level_end: newData.level_end || 1
    }
    // create the hash for the entry
    tmp.id = tmp.id = `${user.id}_${season}`

    return await pg.update_bulk(Season_User_Config, [tmp])
        .then(()=> {return {status: "success", success: "added"}})
        .catch(err => {console.log(err); return {status: "error", error: "failed to add"}})


}