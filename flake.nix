{
  description = "Fo76";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.11";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system:
    let

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      package_name = "api_fo76";
      # name in teh package.json
      package_name_alt_back = "fo76_market_backend";
      package_name_alt_front = "trading_frontend";

      # set the node version here
      nodejs = pkgs.nodejs-14_x;

    in rec {

      packages = {
        backend = pkgs.mkYarnPackage {
              name = "${package_name}_backend";

              src = ./backend;
              packageJSON = "${./backend/package.json}";
              yarnLock = "${./backend/yarn.lock}";
              buildPhase = "yarn build";
              
              installPhase = ''
                runHook preInstall

                mv deps/${package_name_alt_back} $out
                rm $out/node_modules
                cp -R node_modules/. $out/node_modules

                runHook postInstall
              '';
              
              # I just dont want it to run
              distPhase = ''
              
              '';
              
            };
      
        frontend = pkgs.mkYarnPackage {
            name = "${package_name}_frontend";
 
            src = ./frontend;
            packageJSON = "${./frontend/package.json}";
            yarnLock = "${./frontend/yarn.lock}";
            
            # https://git.clerie.de/clerie/nixfiles/src/commit/b1b57a97ad6ea6c0be8154663b2e991c93797d17/pkgs/wetter/default.nix
            # magic
            pkgConfig.node-sass = {
              buildInputs = [ pkgs.python2 pkgs.libsass pkgs.pkgconfig ];
              postInstall = ''
                mkdir -p $HOME/.node-gyp/${nodejs.version}
                echo 9 > $HOME/.node-gyp/${nodejs.version}/installVersion
                ln -sfv ${nodejs}/include $HOME/.node-gyp/${nodejs.version}
                LIBSASS_EXT=auto yarn --offline run build
                rm build/config.gypi
              '';
            };
            
            
            # this does not seem to work
            doDist = false;
            
            
            # this seems to work well for react.js
            buildPhase = ''
              runHook preBuild
              shopt -s dotglob

              rm deps/${package_name_alt_front}/node_modules
              mkdir deps/${package_name_alt_front}/node_modules
              
              pushd deps/${package_name_alt_front}/node_modules
                ln -s ../../../node_modules/* .
              popd
              
              cd deps/${package_name_alt_front}
              
              yarn --offline build
              
              cd ../../
              
              runHook postBuild
            '';
            
            installPhase = ''
              runHook preInstall

              mv deps/${package_name_alt_front}/build $out

              runHook postInstall
            '';
            
            # I just dont want it to run
            distPhase = ''
            
            '';
          };
      };

      defaultPackage = packages.backend;

      nixosModule = { lib, pkgs, config, ... }:
        with lib;
        let
          cfg = config.services."${package_name}";
          
          root_dir = self.defaultPackage."${system}";
        in {
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";

            config = mkOption rec {
              type = types.str;
              default = "./.env";
              example = default;
              description = "The env file";
            };

            prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
            };

            user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
            };

          };

          config = mkIf cfg.enable {

            systemd.services = {
              "${cfg.prefix}${cfg.user}" = {
                description = "Fo76 API";

                wantedBy = [ "multi-user.target" ];
                after = [ "network-online.target" ];
                wants = [ ];
                serviceConfig = {
                  # fill figure this out in teh future
                  DynamicUser=true;
                  Restart = "always";
                  ExecStart = "${nodejs}/bin/node ${root_dir}";
                  WorkingDirectory="${root_dir}";
                  EnvironmentFile = "${cfg.config}";
                };
              };
            };

   
          };

        };

    });
}