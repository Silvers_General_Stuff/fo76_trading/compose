import React, { Component } from "react";
import { Accordion, AccordionItem } from "carbon-components-react";

export class Home extends Component <any, any>{
    render() {
        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <div className="bx--col bx--no-gutter">
                    <h1>
                        Fo76 Tools
                    </h1>
                </div>
            </div>
            <div className="bx--row main_body__r2">
                <div className="bx--col bx--no-gutter">
                    <Accordion align='start'>
                        <AccordionItem open title="Steam Login">
                            <p>
                                The suite originally released with Steam providing authentication, then based
                                on <a href={"https://www.reddit.com/r/fo76/comments/mf0p5a/tool_a_season_tracker/"} target={"_blank"} rel={"noreferrer noopener"} >feedback</a> I removed it.
                                <br />
                                What followed it turned out to be a complicated mess to both use and to code for.
                                <br />
                                So I re-implemented the steam Oauth, any existing data will be migrated over to it.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Technical Stuff">
                            <p>
                                I wanted to experiment with using both Oauth, Docker and Postgres as well as something that looks better than Bootstrap.
                                <br />
                                To replace Bootstrap I settled on <a href={"https://www.carbondesignsystem.com/"} target={"_blank"} rel={"noreferrer noopener"} >IBM's Carbon Design</a>
                                along with the <a href={"https://github.com/carbon-design-system/carbon-addons-iot-react"} target={"_blank"} rel={"noreferrer noopener"} >IOT addon</a> which extends the carbon components.
                                <br />
                                All the code is available <a href={"https://gitlab.com/Silvers_General_Stuff/fo76_trading/compose"} target={"_blank"} rel={"noreferrer noopener"} >on my gitlab.</a>
                                if you want to check it out.
                                <br />
                                So far it was fun building this and putting everything together.

                                <br />
                                <br />
                                With the revamp I moved the DB over to my main MongoDB server as well as migrate the entire infrastructure to NixOS.
                            </p>
                        </AccordionItem>
                        <AccordionItem title="There are bugs?">
                            <p>
                                Please open an issue on <a href={"https://gitlab.com/Silvers_General_Stuff/fo76_trading/compose/-/issues"} target={"_blank"} rel={"noreferrer noopener"} >gitlab.</a>
                            </p>
                        </AccordionItem>
                        <AccordionItem title="Who are you?">
                            <p>
                                On discord I am Silver#5563, you can find me in teh fallout community server <a href={`https://discord.gg/Pfq4X6nnfU`} target={"_blank"} rel={"noreferrer noopener"} >or my own</a>
                            </p>
                        </AccordionItem>
                    </Accordion>

                </div>
            </div>
        </div>
    }
}