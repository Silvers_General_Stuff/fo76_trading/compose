// @ts-ignore
import RemoteStorage from "remotestoragejs";

import {Season, Season_Config, Season_Rewards, Season_User, TaskList_Entry} from "./interfaces"


async function get_data_array<T>(privateClient: any, path: string): Promise<T[]>{
    // return time from getAll is unknown
    return await privateClient.getAll(path)
        .then((result_object_unknown: unknown) => {
            let result_object = result_object_unknown as {[propName: string]: T};
            return Object.values(result_object)
        })
        .catch((err:any) => {
            console.log(err);
            return [] as T[]
        })
}

// https://json-schema.org/
let season_user_data_builder = (privateClient:any) => {
    // let table = "season_user";

    let progression_schema = {
        type: "object",
        properties: {
            id: {
                type: "string",
                required: true
            },
            season: {
                type: "number",
                required: true
            },
            date_updated: {
                type: "string",
                required: true
            },
            level: {
                type: "number",
                required: true,
                default: 0
            }
        }
    }

    privateClient.declareType('season_user_progression',progression_schema);

    return {
        exports: {
            get: (season_number: number) => get_data_array<Season_User>(privateClient, `${season_number}/`)
        }
    }
}

const DB_Season_User = { name: "fo76_ie/season_user", builder: season_user_data_builder };


// this is the offline cache of the seasond atra
let season_builder = (privateClient:any, publicClient:any) => {
    let season_schema = {
        type: "object",
        properties: {
            season: {
                type: "number",
                required: true
            },
            name: {
                type: "string",
                required: true
            },
            start: {
                type: "string",
                required: true
            },
            end: {
                type: "string",
                required: true
            },
            end_final: {
                type: "string",
                required: true
            },
            base: {
                type: "number",
                required: true
            },
            extra: {
                type: "number",
                required: true
            },
            ratio: {
                type: "number",
                required: true
            },
            atoms: {
                type: "number",
                required: true
            },
            score_daily: {
                type: "number",
                required: true
            },
            score_daily_nw: {
                type: "number",
                required: true
            },
            score_weekly: {
                type: "number",
                required: true
            },
            score_level_board: {
                type: "number",
                required: true
            },
            score_level_max: {
                type: "number",
                required: true
            }
        }
    }

    privateClient.declareType('season_object',season_schema);
    publicClient.declareType('season_object',season_schema);

    return {
        exports: {
            get: () => get_data_array<Season>(privateClient, ``)
        }
    }
}

const DB_Season = { name: "fo76_ie/season", builder: season_builder };

let season_reward_builder = (privateClient:any) => {
    let season_reward_schema = {
        type: "object",
        properties: {
            id: {
                type: "string",
                required: true
            },
            first: {
                type: "boolean",
                required: true
            },
            season: {
                type: "number",
                required: true
            },
            level: {
                type: "number",
                required: true
            },
            name: {
                type: "string",
                required: true
            },
            type: {
                type: "string",
                required: true
            },
            quantity: {
                type: "number",
                required: true
            },
            rarity: {
                type: "string",
                required: true
            }
        }
    }

    privateClient.declareType('season_reward_object',season_reward_schema);

    return {
        exports: {
            get: (season_number: number) => get_data_array<Season>(privateClient, `${season_number}/`)
        }
    }
}

const DB_Season_Reward = { name: "fo76_ie/season_reward", builder: season_reward_builder };

let site_config_builder = (privateClient:any) => {
    let schema = {
        type: "object",
        properties: {
            page: {
                type: "string"
            },
            idiot_savant: {
                type: "boolean"
            },
            date_start: {
                type: ["string", "null"]
            },
            level_start: {
                type: "number"
            },
            level_end: {
                type: "number"
            }
        }
    }

    privateClient.declareType('config_object',schema);

    let get = async (page: string): Promise<Season_Config[]> =>{
        let path = `${page}`;

        return await privateClient.getObject(path)
            .then((result:Season_Config) =>{
                if(result){
                    return [result]
                }else{
                    return []
                }
            })
            .catch((err: any) => {
                console.log(err);
                return []
            })
    }

    return {
        exports: {
            get: get
        }
    }
}

const DB_Site_Config = { name: "fo76_ie/config", builder: site_config_builder };


let task_list_builder = (privateClient:any) => {
    let characters_schema = {
        type: "array",
        items: {
            type: "string"
        },
        default: []

    }

    privateClient.declareType('characters_object',characters_schema);

    // get the lsit of characters
    let characters_get = async(): Promise<string[]> =>{
        return await privateClient.getObject(`characters`)
            .then((result: any) => {
                if (result) {
                    return result
                } else {
                    return []
                }
            })
            .catch((err: any) => {
                console.log(err);
                return []
            })
    }

    let task_schema = {
        type: "object",
        properties: {
            id: {
                type: "number"
            },
            character: {
                type: "string"
            },
            name: {
                type: "string"
            },
            date: {
                type: "string"
            }
        }
    }
    privateClient.declareType('task_object',task_schema);

    let task_get = async (task:TaskList_Entry): Promise<TaskList_Entry[]> =>{
        let path = `${task.character}/`;

        return get_data_array(privateClient, path)
    }

    return {
        exports: {
            characters: {
                get: characters_get
            },
            task: {
                get: task_get
            }
        }
    }
}

const DB_task_list_builder_Config = { name: "fo76_ie/task_list", builder: task_list_builder };


let modules = [DB_Season_User, DB_Season, DB_Season_Reward, DB_Site_Config, DB_task_list_builder_Config];


interface LocalDB_Functions<T> {
    get(): T[];
}
// same as above but the get passes a season number
interface LocalDB_Functions_Season<T> {
    get(season_number: number | string): T[];
}

interface LocalDB_Functions_Season_Config {
    get<T>(season_number: number | string): T[];
}

interface LocalDB_General {
    get<T>(search: T): Promise<T[]>;
}

interface LocalDB_Functions_TaskList {
    characters: LocalDB_General,
    task: LocalDB_General,
}

export interface LocalDB extends RemoteStorage {
    "fo76_ie/season": LocalDB_Functions<Season>;
    "fo76_ie/season_user": LocalDB_Functions_Season<Season_User>;
    "fo76_ie/season_reward": LocalDB_Functions_Season<Season_Rewards>;
    "fo76_ie/config": LocalDB_Functions_Season_Config;
    "fo76_ie/task_list": LocalDB_Functions_TaskList;
}

export function leftDB():LocalDB {
    const remoteStorage = new RemoteStorage({
        //logging: true,
        modules:modules
    });

    // add keys for linking to dropbox and google drive
    // @ts-ignore
    remoteStorage.setApiKeys({googledrive: '326492793124-55p5kktfuvi4sc84mvnbcmenbrncjnbi.apps.googleusercontent.com', dropbox: "umwpn1ku12p8asz"});

    // add the caching for all the data here
    remoteStorage.access.claim('fo76_ie', 'rw')
    remoteStorage.caching.enable('/fo76_ie/')

    // for missing module types
    // @ts-ignore
    return remoteStorage
}

