import React, {Component} from "react";
import {Data_User_Account_Data, TaskList_Entry, TaskList_Entry_Reset_Type} from "../config/interfaces"
import {
    Button,
    Column,
    Dropdown,
    Form,
    Grid,
    Row, StructuredListBody, StructuredListCell, StructuredListHead, StructuredListRow, StructuredListWrapper,
    TextInput
} from 'carbon-components-react';
import {get_data, post_data} from "./seasons";


// what gets passed in
interface TaskList_Props {
    backend: string;
    user: Data_User_Account_Data
}
// what it uses
interface TaskList_State {
    backend: string;
    user: Data_User_Account_Data
    // active character
    character?: string;
    character_tmp: string;

    // list of all characters
    characters: string[];

}

export class TaskList extends Component <TaskList_Props, TaskList_State>{
    constructor(props: TaskList_Props) {
        super(props);

        let user = this.props.user;

        let characters = user.characters;
        let character = undefined;
        if(characters.length > 0){
            character = characters[0]
        }

        this.state = {
            backend: this.props.backend,
            user: user,
            character: character,
            character_tmp: "",
            characters: characters,
        }
    }

    componentDidMount = async() =>{ }

    handle_dropdown = () =>{
        let { characters, character } = this.state;

        return <Dropdown
            ariaLabel="Task List Character"
            id="task_list-character-dropdown"
            items={characters}
            selectedItem={character}
            onChange={(result)=>{
                if(result.selectedItem){
                    this.setState({character: result.selectedItem })
                }
            }}
            label="Please add a Character"
            titleText=""
        />
    }

    handle_add = () =>{
        let { character_tmp } = this.state;

        return <Form
            onSubmit={async(e)=>{
                e.preventDefault();

                let {character_tmp, backend, user, characters} = this.state;

                // do nothing if its undefiend or blank
                if(character_tmp === ""){return}

                characters.push(character_tmp)

                let tmp = {
                    id: user.id,
                    characters: characters,
                }

                // update server
                await post_data("POST", `${backend}/account`, tmp)

                this.setState({
                    character_tmp: "",
                    character: character_tmp,
                    characters: characters
                })
            }}
        >
            <TextInput
                id="task_list-character-add"
                value={character_tmp}
                onChange={(event)=>{
                    this.setState({character_tmp:event.target.value})
                }}
                labelText=""
                placeholder="Add Character"
            />
        </Form>
    }

    handle_delete = () =>{
        let { character} = this.state;

        // only show if there is a character
        if(!character){ return null}

        return <Button
            kind='danger'
            // gotta overide it, for some reason teh default is transparent
            style={{"background-color": "#da1e28"}}
            onClick={async()=>{
                let { backend, user, characters} = this.state;
                if(!character){return}
                characters = characters.filter(item =>item !== character)

                let new_main = undefined;
                // set the characters
                if(characters.length >0){
                    new_main = characters[0]
                }

                let tmp = {
                    id: user.id,
                    characters: characters,
                }

                await post_data("POST", `${backend}/account`, tmp)
                // delete all lists associated with teh character
                await post_data("DELETE", `${backend}/tasks/${character}`, ["all"]);

                this.setState({
                    character: new_main,
                    characters: characters,
                })
            }}
        >
            Delete {character}
        </Button>
    }


    render() {
        let {character, backend, user} = this.state;
        let tasks = null;
        if(character){
            tasks = <TaskListList backend={backend} character={character} user={user} />
        }



        return <Grid fullWidth >
            <Row>
                <Column>
                    <h1>Task List</h1>
                </Column>
            </Row>
            <Row>
                <br />
            </Row>
            <Row>
                <Column sm={4} md={4} lg={4}>
                    {this.handle_dropdown()}
                    <br />
                </Column>
                <Column sm={6} md={6} lg={6}>
                    {this.handle_add()}
                    <br />
                </Column>
                <Column sm={4} md={4} lg={4}>
                    {this.handle_delete()}
                    <br />
                </Column>
            </Row>
            <Row>
                <Column sm={4} md={4} lg={4}>
                </Column>
                <Column sm={6} md={6} lg={6}>
                    {tasks}
                </Column>
                <Column sm={4} md={4} lg={4}>

                </Column>
            </Row>
        </Grid>
    }
}


// what gets passed in
interface TaskList_List_Props {
    backend: string;
    character: string;
    user: Data_User_Account_Data
}
// what it uses
interface TaskList_List_State {
    backend: string;
    character: string;
    tasks: TaskList_Entry[];
    user: Data_User_Account_Data
}

// this manages the tabs
export class TaskListList extends Component <TaskList_List_Props, TaskList_List_State>{
    constructor(props: TaskList_List_Props) {
        super(props);
        this.state = {
            backend: this.props.backend,
            character: this.props.character,
            user: this.props.user,
            tasks: [],
        }
    }

    componentDidMount = async() => {
        let { character} = this.state;
        this.setState({tasks: await this.get_tasks(character)})
    }

    componentDidUpdate = async(prevProps: TaskList_List_Props) => {
        if(prevProps.character !== this.props.character){
            this.setState({
                character: this.props.character,
                tasks: await this.get_tasks(this.props.character),
            })
        }
    }

    get_tasks = async (character: string): Promise<TaskList_Entry[]> =>{
        let {backend, user} = this.state;

        let tasks = await get_data<TaskList_Entry>(`${backend}/tasks/${character}`);

        let template: TaskList_Entry[] = [
            { id: `${user.id}_${character}_0`,  user: user.id, order: 1,  name: "Daily - OP", character: character, date: new Date(1).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 17 } },
            { id: `${user.id}_${character}_1`,  user: user.id, order: 2,  name: "Daily - Faction - Foundation", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 0 } },
            { id: `${user.id}_${character}_2`,  user: user.id, order: 3,  name: "Daily - Faction - Raider", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 0 } },
            { id: `${user.id}_${character}_3`,  user: user.id, order: 4,  name: "Daily - Faction - Raider - Ohio", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 0 } },
            { id: `${user.id}_${character}_4`,  user: user.id, order: 5,  name: "Daily - Faction - Overseer", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 0 } },
            { id: `${user.id}_${character}_5`,  user: user.id, order: 6,  name: "Daily - Ally", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Offset, value: 24 } },
            { id: `${user.id}_${character}_6`,  user: user.id, order: 7,  name: "Weekly - Smiley - Gold", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Day, value: 1, value_secondary: 0 } },
            { id: `${user.id}_${character}_7`,  user: user.id, order: 8,  name: "Vendor - Caps", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Offset, value: 20 } },
            { id: `${user.id}_${character}_8`,  user: user.id, order: 9,  name: "Vendor - Scrip", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Offset, value: 20 } },
            { id: `${user.id}_${character}_9`,  user: user.id, order: 10, name: "Vendor - Gold", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Offset, value: 20 } },
            { id: `${user.id}_${character}_10`, user: user.id, order: 11, name: "Daily - Challenges", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Hour, value: 16 } },
            { id: `${user.id}_${character}_11`, user: user.id, order: 12, name: "Weekly - Challenges", character: character, date: new Date(0).toISOString(),offset: {type: TaskList_Entry_Reset_Type.Day, value: 2, value_secondary: 16 } },
        ]

        let total = [];
        let to_update = [];
        for(let item of template){
            let pair = tasks.filter(task => task.order === item.order)

            // use existing date
            if(pair.length >0){
                item.date = pair[0].date
            } else{
                to_update.push(item)
            }

            total.push(item)
        }

        // add the updates
        if(to_update.length >0){
            await post_data("POST", `${backend}/tasks/${character}`, to_update);
        }

        return total
    }

    create_table = () =>{
        let {backend, tasks} = this.state;

        tasks.sort(( a, b ) =>{
            if ( a.name < b.name ){return -1}
            if ( a.name > b.name ){return 1}
            return 0;
        })

        let rows = [];
        for(let i=0;i< tasks.length;i++){
            rows.push(<TaskListTimer backend={backend} task_data={tasks[i]}/>)
        }


        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListHead>
                <StructuredListRow
                    head
                    tabIndex={0}
                >
                    <StructuredListCell head>
                        Activity
                    </StructuredListCell>
                    <StructuredListCell
                        head
                        style={{textAlign: "right"}}
                    >
                        Available
                    </StructuredListCell>
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                {rows}
            </StructuredListBody>
        </StructuredListWrapper>
    }

    render() {
        return this.create_table()
    }
}


// what gets passed in
interface TaskList_Timer_Props {
    backend: string;
    task_data: TaskList_Entry;
}
// what it uses
interface TaskList_Timer_State {
    backend: string;
    task_data: TaskList_Entry;
    ready: string;
    date: number;
    now: number;
}
// this manages the tabs
export class TaskListTimer extends Component <TaskList_Timer_Props, TaskList_Timer_State>{
    private timerID: NodeJS.Timeout | undefined;
    constructor(props: TaskList_Timer_Props) {
        super(props);

        let task_data = this.props.task_data;
        let ready = this.process_data(task_data);

        this.state = {
            backend: this.props.backend,
            task_data: task_data,
            ready: ready,
            date: new Date(ready).getTime(),
            now: new Date().getTime()
        }
    }

    componentDidMount() {
        // start the timer
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate = async(prevProps: TaskList_Timer_Props) => {
        if(prevProps.task_data.character !== this.props.task_data.character){

            let task_data = this.props.task_data;
            let ready = this.process_data(task_data);
            this.setState({
                task_data: task_data,
                ready: ready,
                date: new Date(ready).getTime()
            })
            if(!this.timerID){
                this.timerID = setInterval(
                    () => this.tick(),
                    1000
                );
            }

        }
    }

    componentWillUnmount() {
        if(this.timerID){
            clearInterval(this.timerID);
        }
        this.timerID = undefined;
    }

    process_data = (task_data:  TaskList_Entry): string =>{
        let {date, offset} = task_data;

        let ready: string;
        switch(offset.type){
            case TaskList_Entry_Reset_Type.Day : {
                let now_task = new Date(date);

                // get the day assosiated witht eh task
                let day_task = now_task.getUTCDay()

                let offset_hours = 0;
                if(offset.value_secondary){
                    offset_hours = offset.value_secondary;
                }

                // wipe the hrs/min/seconds
                now_task.setUTCHours(offset_hours, 0, 0, 0)

                // calculate teh offset to teh next time it resets
                let offset_value = (offset.value - day_task) % 7;
                if(offset_value <= 0){
                    offset_value += 7
                }

                now_task.setUTCDate(now_task.getUTCDate() + offset_value)

                ready = now_task.toISOString();

                break
            }

            case TaskList_Entry_Reset_Type.Hour : {
                let now_task = new Date(date);

                // get the day assosiated witht eh task
                let hours_task = now_task.getUTCHours()

                // calculate teh offset to teh next time it resets
                let offset_value = (offset.value - hours_task) % 24;

                if(offset_value <= 0){
                    offset_value += 24
                }

                // wipe the hrs/min/seconds
                now_task.setUTCHours(now_task.getUTCHours() + offset_value, 0, 0, 0)

                ready = now_task.toISOString();

                break
            }

            case TaskList_Entry_Reset_Type.Offset : {
                //simply offset the number of hours

                let now_task = new Date(date);
                now_task.setUTCHours(now_task.getUTCHours() + offset.value)

                ready = now_task.toISOString();

                break
            }

            default: {
                ready = new Date(0).toISOString();
            }
        }

        return ready;
    }

    tick() {
        let {date, now} = this.state;
        if(now < date){
            this.setState({now: new Date().getTime()});
        } else{
            if(this.timerID){
                clearInterval(this.timerID);
                this.timerID = undefined;
            }
        }
    }

    secondsToTime = (milliseconds: number): string =>{
        let secs = milliseconds/1000;

        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = `0${Math.floor(divisor_for_minutes / 60)}`.slice(-2);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = `0${Math.ceil(divisor_for_seconds)}`.slice(-2);

        return `${hours}:${minutes}:${seconds}`
    }

    create_row = () =>{
        let {ready, date, now, task_data, backend} = this.state;

        let available: string;
        let background: string | undefined;
        if(now < date){
            // return countdown
            available = this.secondsToTime(date - now)
            background = undefined;
        }else{
            available =  "Ready";
            background = "rgba(99, 173, 82, 0.2)";
        }

        return  <StructuredListRow
            tabIndex={0}
            style={{
                cursor: "pointer",
                backgroundColor: background
            }}
            // update data here
            onClick={async ()=>{
                let now_update = new Date().toISOString();

                if(now_update > ready){
                    // update with new time
                    task_data.date = now_update;
                } else{
                    // reset the time
                    task_data.date = new Date(0).toISOString();
                }

                await post_data("POST", `${backend}/tasks/${task_data.character}`, [task_data]);

                // update state
                let ready_new = this.process_data(task_data);

                this.setState({
                    ready: ready_new,
                    date: new Date(ready_new).getTime()
                })

                if(!this.timerID){
                    this.timerID = setInterval(
                        () => this.tick(),
                        1000
                    );
                }

            }}
        >
            <StructuredListCell>
                {task_data.name}
            </StructuredListCell>
            <StructuredListCell
                style={{textAlign: "right"}}
            >
                {available}
            </StructuredListCell>
        </StructuredListRow>


    }

    render() {
        return this.create_row()
    }
}
