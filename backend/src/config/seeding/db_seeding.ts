import { PG_Manager } from "../db";
import { Season, Season_Events, Season_Rewards } from "../db_entities"
import { seed_season, seed_season_events, seed_season_rewards } from "./seed_season"
import Hasher from 'object-hash';


export async function controller_seeding(pg: PG_Manager){
    await controller_season(pg);
}

async function controller_season(pg: PG_Manager){
    await season_season(pg);
    await season_season_events(pg);
    await season_season_rewards(pg);
}

async function season_season(pg: PG_Manager){
    await pg.update_bulk(Season, seed_season)
}

async function season_season_events(pg: PG_Manager){
    await pg.update_bulk(Season_Events, seed_season_events)
}

async function season_season_rewards(pg: PG_Manager){
    // create ID based on teh data
    seed_season_rewards.map(item => {item.id = Hasher(item)})

    // delete all existing ones to start fresh
    await pg.delete(Season_Rewards, {})

    await pg.update_bulk(Season_Rewards, seed_season_rewards)
}
