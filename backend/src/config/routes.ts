import {Ctx} from "../index";

import auth from "../routes/auth"
import account from "../routes/account"
import season from "../routes/season"
import tasks from "../routes/tasks"

export default (ctx:Ctx) => {
    auth(ctx);
    account(ctx);
    season(ctx);
    tasks(ctx);
}