import dotenv from 'dotenv'
import packageData from '../../package.json'

dotenv.config();


interface Config {
  name: string;
  version: string
  env: string
  port: number
  frontend: string
  db:{
    db_name: string
    db_port: number
    db_host: string
    db_user: string
    db_pass: string
  }
  sessionSecret: string
  jwtSecret: string
  sso:{
    [propName: string]: SSO
  }
}

interface SSO {
  id: string
  secret: string
  url: string
}

const config: Config = {
  name: packageData.name,
  version: packageData.version,
  env: process.env.NODE_ENV || process.env.ENVIROMENT,
  port: parseInt(process.env.REACT_APP_BACKEND_PORT, 10) || 8080,
  frontend: process.env.REACT_APP_FRONTEND_ADDRESS,
  db: {
    db_name: process.env.DB,
    db_port: parseInt(process.env.DB_PORT) || 5432,
    db_host: process.env.DB_LOCATION,
    db_user: process.env.DB_USER,
    db_pass: process.env.DB_PASS,
  },
  sessionSecret: process.env.SECRET_SESSION,
  jwtSecret: process.env.SECRET_JWT,
  sso: {
    steam: {
      id: process.env.OAUTH_STEAM_ID,
      secret: process.env.OAUTH_STEAM_SECRET,
      url: process.env.OAUTH_STEAM_CALLBACK
    },
  }
};
export default config
