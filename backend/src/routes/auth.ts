import jwt from 'jsonwebtoken'
import config from "../config/config";
import {Ctx} from "../index"


export default (ctx:Ctx) => {
    const {server, passport} = ctx

    let success = config.frontend + "#/";
    // redirects back to teh sync page
    let failure = config.frontend + "#/link";

    server.get('/auth/steam',processGeneral(passport, 'steam'));
    server.get('/auth/steam/callback', processGeneralCallback(passport, "steam", failure), redirectWithToken(success));

}

const processGeneral = (passport, provider:string, object?:object) => {
    if(object){
        return passport.authenticate(provider, object)
    }
    return  passport.authenticate(provider)
};

const processGeneralCallback = (passport, provider:string, failure:string) => {
    return  passport.authenticate(provider, { failureRedirect: failure })
};

let maxAge = 365*24*60*60*1000

const redirectWithToken =  (success:string) =>{
    return (req, res) => {
        return res
            .status(200)
            // this is the actual jwt
            .cookie('jwt', signToken(req.user.id), {
                domain:"fo76.ie",
                httpOnly: true,
                secure: true,
                maxAge: maxAge,
                //sameSite: "Lax"
            })
            // this is to let the browser know that the user is logged in, no other purporse
            .cookie('loggedIn', true, {
                domain:"fo76.ie",
                httpOnly: false,
                secure: true,
                maxAge: maxAge,
                //sameSite: "Lax"
            })
            .redirect(success)
    }
};

const signToken = user => {
    return jwt.sign({ id: user }, config.jwtSecret, {expiresIn: maxAge})
};