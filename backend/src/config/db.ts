import "reflect-metadata";
import {Connection, createConnection, DeepPartial, EntitySchema, FindConditions} from "typeorm";

import config from "./config";
import { entities } from "./db_entities";
import { controller_seeding } from "./seeding/db_seeding";

export interface UpdateOne<T>{
    updateOne: {
        filter: {
            [propName: string]: any
        }
        update: {
            $set: DeepPartial<T>
            $setOnInsert?: DeepPartial<T>
        }
        upsert: boolean
    }
}
export interface DeleteOne<T>{
    deleteOne: {
        filter: {
            [propName: string]: any
        }
    };
}
export interface BulkArray<T>{
    arrays: number
    [propName: number]: (UpdateOne<T>| DeleteOne<T>)[]
}

export class PG_Manager {
    public connection: Connection;

    constructor(){}

    public async up(){
        console.time("DB: Connecting");
        this.connection = await createConnection({
            name: new Date().toISOString(),
            type: "mongodb",
            url: `mongodb+srv://${config.db.db_user}:${config.db.db_pass}@${config.db.db_host}/${config.db.db_name}`,
            ssl: false,
            database: config.db.db_name,
            entities: entities,
            useNewUrlParser: true,
            useUnifiedTopology: true,
            logging: false,
            extra: {
                max: 50
            },
        }).catch(error => {
            console.log(error);
            process.exit(1)
        })
        console.timeEnd("DB: Connecting");

        await this.create_collections()
    }

    public async down(){
        await this.connection.close();
    }

    private async create_collections(){
        console.time("DB: Creating");
        // mongodb has issues with syncronise
        try {
            await this.connection.synchronize();
        } catch(e){
            if(e.message.indexOf("MongoError: Index keys cannot be empty.") >= 0){
                console.log(e)
            } else{
                console.info("Index errors")
            }
        }
        /*
        for(let entity of entities){
            let repo = this.connection.getMongoRepository(entity);
            let data = repo.metadata;

            if(data.name === "TaskList_Entry"){
                console.log(data.name)

                for(let index of data.ownIndices){
                    console.log(index.isUnique, index.givenColumnNames)
                }
            }
        }
         */
        console.timeEnd("DB: Creating");
    }

    public async seed(){
        console.time("DB: Seeding");
        await controller_seeding(this)
        console.timeEnd("DB: Seeding");
    }

    public async update_one<T>(repo: string | Function | (new () => T) | EntitySchema<T>, item: DeepPartial<T>){
        await this.connection.getMongoRepository<T>(repo).save(item)
    }

    public async delete<T>(repo: string | Function | (new () => T) | EntitySchema<T>, item: FindConditions<T>){
        await this.connection.getMongoRepository<T>(repo).delete(item);
    }

    // takes an array of items, converts it to
    private static bulk_update_array<T> (data:Array<DeepPartial<T>>, limit:number= 100000, accessor:string = "id", setOnInsert?:DeepPartial<T>): BulkArray<T> {
        let tmp: BulkArray<T> = { arrays: 0, 0: [] };
        for (let j = 0; j < data.length; j++) {
            let temp: UpdateOne<T> = {
                updateOne: {
                    filter:{
                        [accessor]: data[j][accessor]
                    },
                    update: {
                        $set: data[j]
                    },
                    upsert: true
                }
            };

            if (setOnInsert) {
                temp.updateOne.update["$setOnInsert"] = setOnInsert
            }
            if (tmp[tmp.arrays].length < limit) {
                tmp[tmp.arrays].push(temp)
            } else {
                tmp.arrays += 1;
                tmp[tmp.arrays] = [temp]
            }
        }
        return tmp
    }

    public async update_bulk<T>(repo: string | Function | (new () => T) | EntitySchema<T>, items: DeepPartial<T>[], limit: number = 100000){
        let repository = this.connection.getMongoRepository<T>(repo);

        let default_id = "id";
        let indexes = repository.metadata.indices.filter(index => index.isUnique)
        if(indexes.length > 0){
            if(indexes[0].givenColumnNames.length > 0){
                default_id =indexes[0].givenColumnNames[0]
            }
        }

        let processed = PG_Manager.bulk_update_array(items, limit, default_id)
        for (let i = 0; i < processed.arrays + 1; i++) {
            if (processed[i].length > 0) {
                await repository.bulkWrite(processed[i], { ordered: false }).catch((err) => console.log(repo, err,location))
            }
        }
    }

    public async get_items<T>(repo: string | Function | (new () => T) | EntitySchema<T>, query: DeepPartial<T>, project?: string[]):Promise<T[]>{
        let repository = this.connection.getMongoRepository<T>(repo);

        let find_options = {where: query}
        if (project){
            find_options["select"] = project
        }

        return await repository.find(find_options).catch(err=> {console.log(err); return []})
    }
}
