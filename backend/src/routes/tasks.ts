import { Ctx } from "../index"
import {User,  TaskList_Entry} from "../config/db_entities";
import { PG_Manager } from "../config/db";
import { Result_Object } from "./account";

export default (ctx: Ctx) => {
    const {passport, pg, server} = ctx

    // authenticated
    // gets the users records for a season
    server.get('/tasks/:character', passport.authenticate('jwt', { session: false }), async (req, res) => {
        let user = req.user as User;
        let result = await pg.get_items(TaskList_Entry, {user: user.id, character: req.params.character})
        res.send(result);
    })

    // sets the users records for a season
    server.post('/tasks/:character', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        // body  is  TaskList_Entry[]
        let result = await task_add(pg, req.user as User, req.body);
        res.send(result);
    })

    server.delete('/tasks/:character', passport.authenticate('jwt', { session: false }), async (req, res) => {
        if(typeof req.body === "undefined"){
            return res.send({result: "error", error: "no body"})
        }

        // body  is  strings[] eitehr all or the id of the object
        let result = await task_delete(pg, req.user as User,  req.body, req.params.character);
        res.send(result);
    })

}

async function task_add (pg: PG_Manager, user:User, newData: TaskList_Entry[]): Promise<Result_Object<string>>{
    let for_update = newData.filter(task => task.user === user.id);
    return await pg.update_bulk(TaskList_Entry, for_update)
        .then(()=> {return {status: "success", success: "added"}})
        .catch(err => {console.log(err); return {status: "error", error: "failed to add"}})
}

async function task_delete (pg: PG_Manager, user:User,  newData: string[], character: string): Promise<Result_Object<string>>{
    let repo = await pg.connection.getMongoRepository(TaskList_Entry)

    if(newData.length === 1 && newData[0] === "all"){
        return await repo.deleteMany({user: user.id, character: character})
            .then(()=> {return {status: "success", success: "deleted All"}})
            .catch(err => {console.log(err); return {status: "error", error: "failed to delete"}})
    }else {
        await Promise.all(newData.map(async(id) => await repo.deleteOne({id: id, user: user.id, character: character})))
            .then(()=> {return {status: "success", success: "deleted some"}})
            .catch(err => {console.log(err); return {status: "error", error: "failed to delete"}})
    }
}