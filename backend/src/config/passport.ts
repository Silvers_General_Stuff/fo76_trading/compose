import passport, {PassportStatic} from "passport"
import config from "./config";
import {Strategy as Strategy_Steam} from "passport-steam"
import {Strategy as Strategy_Jwt} from "passport-jwt"
import { User, User_Steam } from "./db_entities"
import { PG_Manager } from "./db";

export function passportSetup (pg:PG_Manager): PassportStatic{
    // these are used to sign up
    passport.use(oauth_steam(pg));
    // this is used after signup/login to access data
    passport.use(jwtGen(pg));

    passport.serializeUser((user, cb) => {cb(null, user)});
    passport.deserializeUser((obj, cb) => {cb(null, obj)});

    return passport
}

// steam is teh core of teh login
function oauth_steam(pg: PG_Manager) {
    return new Strategy_Steam({
            returnURL: config.sso.steam.url,
            realm: config.sso.steam.id,
            apiKey: config.sso.steam.secret,
        },
        async (identifier, profile_steam: User_Steam, done) => {
            // check to see if teh user already exists
            let user = await pg.get_items(User, {id: profile_steam.id});

            let profile: User;
            // if not then create the profile
            if (user.length === 0) {
                // disabling signups
                await pg.update_one(User_Steam, profile_steam);

                profile = new User()
                profile.id = profile_steam.id
                profile.displayName = profile_steam.displayName
                profile.steam = profile_steam

                await pg.update_one(User, profile);
            } else {
                profile = user[0]
            }
            return done(null, profile)
        })
}

function jwtGen(pg: PG_Manager){
    const cookieExtractor = (req) => {
        let token = null;
        if (req && req.cookies) {
            token = req.cookies['jwt'];
        }
        return token;
    };
    let opts = {
        jwtFromRequest: cookieExtractor,
        secretOrKey: config.jwtSecret,
    };
    return new Strategy_Jwt(opts, async(jwt_payload, done)=> {
        let results = await pg.get_items(User, {id:jwt_payload.id});

        if(results.length === 0){
            return done(new Error("No user"))
        }else{
            return done(null, results[0])
        }
    })
}