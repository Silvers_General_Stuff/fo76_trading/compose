// this one is for both logged in and not users.
// logged in folks get to actually use it

import React, {Component} from "react";
import {
    Season,
    Season_Config,
    Season_Rewards,
    Season_Rewards_Rarity,
    Season_Rewards_Type,
    Season_User
} from "../config/interfaces"
import {
    Accordion,
    AccordionItem,
    DataTable,
    DatePicker,
    DatePickerInput,
    NumberInput,
    StructuredListBody,
    StructuredListCell,
    StructuredListHead,
    StructuredListRow,
    StructuredListWrapper,
    Tab,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableHeader,
    TableRow,
    Tabs,
    Toggle
} from 'carbon-components-react';
import {LineChart} from "@carbon/charts-react";
import {LineChartOptions, ScaleTypes} from "@carbon/charts/interfaces";

export async function get_data<T>(url: string): Promise<T[]> {
    return await fetch(url, {method: 'GET', credentials: 'include'})
        .then(result => {
            if (result.status === 200) {
                return result.json()
            } else {
                return []
            }
        })
        .catch((err) => {
            console.log(err);
            return []
        }) as T[]
}
export async function post_data(method: string, url: string, data: any) {
    return await fetch(url, {
        method: method,
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

interface Score_XP {
    id: string;
    level: number;
    to_next: number;
    total_xp: number;
    percentage_of_total: number;
}

interface Days_Data {
    id: string;
    day: number;
    // to whatever the viewer wants to see
    date: string;
    level: number;
    days_left: number;
}

interface Graph_Data {
    group: string;
    date: string;
    value: number;
}

interface PvX_PvE {
    pve: number;
    pvx: number;
}

interface Create_Data_Challenges {
    challenge_graph: Graph_Data[];
    estimate_final_score: PvX_PvE;
    days_to_complete_user: PvX_PvE;
    days_to_complete: PvX_PvE;
}

interface Create_Data_Estimate {
    score_starting: number;
    date_start: string;
    estimate_graph: Graph_Data[];
    estimate_final_level: number;
    // days to reach that level
    estimate_final_level_days: number;
    days_remaining: number;
    percentage_complete: number;
}

// what gets passed in
interface Season_Props {
    backend: string;
}
// what it uses
interface Season_State {
    backend: string;
    seasons: Season[];
    idiot_savant: boolean;
}

// https://www.w3schools.com/js/js_cookies.asp
function setCookie(cname: string, cvalue: string, exdays: number) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname: string) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export class Seasons extends Component <Season_Props, Season_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            backend: this.props.backend,
            seasons: [],
            idiot_savant: true
        }
    }

    componentDidMount = async() =>{
        let {backend} = this.state;

        // get local and use that as the state
        this.setState({
            seasons: await get_data<Season>(`${backend}/season`),
            idiot_savant: await this.get_idiot_savant()
        })
    }

    get_idiot_savant = async (): Promise<boolean> =>{
        // if it exists then idiot savant is turned off
        return getCookie("idiot_savant") === "" || getCookie("idiot_savant") === "true";
    }

    create_tabs = () =>{
        let {seasons, backend, idiot_savant} = this.state;

        let tabs = [];
        let selected = 0;
        let now = new Date().getTime();
        for(let i=0;i<seasons.length;i++){
            let season = seasons[i];
            if (
                new Date(season.end_final).getTime() > now &&
                new Date(season.start).getTime() < now
            ){
                selected = seasons.length - i -1;
            }

            // by default show the savant ones
            tabs.push(
                <Tab label={`Season ${season.season}`} id={`Tab-sub-${season.season}`} key={`key-Tab-sub-${season.season}`}
                    renderContent={ ({selected}) => {
                        if (selected) {
                            return <TabManager backend={backend} season={season} key={`season-tab-${season.season}`} savant={idiot_savant} />
                        }else {
                            return <div className={`season-tab-${season.season}--visually-hidden`} />
                        }
                    }}
                />
            )

        }

        if(tabs.length === 0){
            return null
        }


        return <Tabs selected={selected}>
            {tabs.reverse()}
        </Tabs>;
    }

    render() {
        let {idiot_savant} = this.state

        let toggle = <Toggle
            toggled={idiot_savant}
            onToggle={
                 ()=>{
                    setCookie("idiot_savant", `${!idiot_savant}`, 720)

                    // save in state
                    this.setState({idiot_savant: !idiot_savant})
                }
            }
            labelText={""}
            id={`Toggle-savant`}
            labelA={"Off"}
            labelB={"On"}
        />


        return <div className="bx--grid bx--grid--full-width main_body">
            <div className="bx--row main_body__banner">
                <div className="main_body">
                    <h1>Season Tracker</h1>
                    Savant Mode: {toggle}
                </div>
            </div>
            {this.create_tabs()}

        </div>
    }
}

interface Score_Object {
    [propName: number]: Score_XP
}

interface Create_Days_Data {
    days_data: Days_Data[];
    graph_target: Graph_Data[];
}

interface TabManager_Props {
    season: Season;
    backend: string;
    savant: boolean;
}
interface TabManager_State {
    season: Season;
    show_all: boolean;
    backend: string;
    user_stats: Season_User[];
    score: Score_Object;
    rewards: Season_Rewards[];
    savant: boolean;
    config: Season_Config;
    pvx_name: string;
}
class TabManager extends Component <TabManager_Props, TabManager_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            // these have defaults
            show_all: false,


            // these dont get updated
            backend: this.props.backend || "https://api.fo76.ie",
            savant: this.props.savant,

            // tehse get updated
            season: this.props.season,

            // controlled by component
            score: {},
            user_stats: [],
            rewards: [],

            config: {
                date_start: "",
                level_end: 1,
                level_start: 1,
                season: this.props.season.season,
                user: ""
            },
            pvx_name: "PvX"
        }
    }

    getPvXName = (season: Season) =>{
        let pvx_name = "PvX";
        if(season.season >= 9){
            pvx_name = "1st";
        }
        return pvx_name;
    }

    componentDidMount = async() =>{
        let {season, backend} = this.state;

        this.setState({
            pvx_name: this.getPvXName(season),
            score: this.calculate_score_required(season),
            user_stats: await this.get_user_stats(),
            rewards: await get_data<Season_Rewards>(`${backend}/season/rewards/${season.season}`),
            config: await this.get_config()
        })
    }

    get_config = async() =>{
        let {season, backend} = this.state;

        let config = await get_data<Season_Config>(`${backend}/season/config/${season.season}`)
        if(config.length > 0){
            return config[0]
        } else {
            // sets a default config for the season
            let tmp: Season_Config = {
                date_start: "",
                level_end: 1,
                level_start: 1,
                season: season.season,
                user: ""
            }
            await post_data("POST", `${backend}/season/config/${season.season}`, tmp)
            return tmp
        }
    }

    get_user_stats = async (): Promise<Season_User[]> => {
        let {season, backend} = this.state;
        return get_data<Season_User>(`${backend}/season/user/${season.season}`)
    }

    componentDidUpdate = async(prevProps: TabManager_Props) => {
        let isSame = Object.entries(prevProps.season).toString() === Object.entries(this.props.season).toString()

        if(!isSame){
            this.setState({
                pvx_name: this.getPvXName(this.props.season),
                season: this.props.season,
                score: this.calculate_score_required(this.props.season)
            })
        }

        if(prevProps.savant !== this.props.savant){
            this.setState({savant: this.props.savant})
        }
    }

    calculate_score_required = (season: Season): Score_Object =>{
        let data: Score_Object = {};

        let total_xp_board = 0;
        let total_xp = 0;
        // first loop is the abse scoreboard
        for(let level=0;level<(season.score_level_board +1);level++){
            let to_next = season.base + (season.extra * (level -1));
            total_xp += to_next;
            if(level<season.score_level_board){
                total_xp_board += to_next;
            }
            data[level] = {
                id: level.toString(),
                level: level,
                to_next: to_next,
                total_xp: total_xp,
                percentage_of_total: 0
            }
        }

        // second loop takes the result of teh final first loop then uses that for teh lvls
        let to_next_post = data[season.score_level_board-1].to_next;
        let total_xp_post = total_xp;
        for(let level=(season.score_level_board);level<(season.score_level_max+1);level++){
            total_xp_post += to_next_post;

            data[level] = {
                id: level.toString(),
                level: level,
                to_next: to_next_post,
                total_xp: total_xp_post,
                percentage_of_total: 0
            }
        }

        // loop back through to set the percentage
        for(let level=1;level<season.score_level_board;level++){
            let percentage = data[level].total_xp/total_xp_board;
            data[level].percentage_of_total = Math.round((percentage + Number.EPSILON) * 10000)/100;
        }

        return data;
    }

    create_score_table = () =>{
        let { score } = this.state;
        let rowData = Object.values(score);
        let headerData = [
            {key: "level", header: "Level"},
            {key: "to_next", header: "To Next"},
            {key: "total_xp", header: "Total"},
            {key: "percentage_of_total", header: "Total %"}
        ]

        return this.table_base(rowData, headerData)
    }

    create_days_table = (row_data: Days_Data[]) =>{
        let headerData = [
            {key: "day", header: "Day"},
            {key: "days_left", header: "Days Left"},
            {key: "date", header: "Date"},
            {key: "level", header: "Guide Level"},
        ]
        return this.table_base(row_data, headerData)
    }

    table_base = (row_data: any[], headerData: {key: string, header: string}[]) =>{
        return <DataTable rows={row_data} headers={headerData}>
            {
                // @ts-ignore
                ({ rows, headers, getHeaderProps, getTableProps }) => (
                    <TableContainer>
                        <Table {...getTableProps()} size='compact'>
                            <TableHead>
                                <TableRow>
                                    {
                                        // @ts-ignore
                                        headers.map((header) => (
                                            <TableHeader {...getHeaderProps({ header })}>
                                                {header.header}
                                            </TableHeader>
                                        ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    // @ts-ignore
                                    rows.map((row) => {
                                        // @ts-ignore
                                        return <TableRow key={row.id}>
                                            {
                                                // @ts-ignore
                                                row.cells.map((cell) => (
                                                    <TableCell key={cell.id}>{cell.value}</TableCell>
                                                ))
                                            }
                                        </TableRow>
                                    })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                )}
        </DataTable>
    }

    update_score = (score_last: number)=>{
        let {season} = this.state;

        // starts off enabled
        let disabled = false

        // check if it is within teh start date
        let now = new Date().toISOString();
        if(now < season.start){
            disabled = true
        }

        // and give a bit of leeway for teh end date
        let three_days = new Date(season.end_final);
        three_days.setDate(three_days.getDate() + 3);
        if(now > three_days.toISOString()){
            disabled = true
        }

        //let min = (score_last-1) >= 0 ? score_last -1 : 0;
        //let max = (score_last+1) <= season.score_level_max ? score_last +1 : season.score_level_max;

        return  <div style={{maxWidth: 175}}>
            <NumberInput
                disabled={disabled}
                id={"score-input"}
                min={0}
                max={season.score_level_max}
                value={score_last}
                label="Your Current Score"
                onChange={async(e) => this.update_score_function(e,score_last )}
                invalidText="Number is not valid"
            />
        </div>
    }

    update_score_function = async (e: any, last_score: number) =>{
        let {season, backend, user_stats, config} = this.state;

        let new_score = parseInt(e.imaginaryTarget.value, 10) || 1;

        // only allow incrimentlining in +/- 1
        //if(new_score > (last_score + 1)){return []}
        //if(new_score < (last_score - 1)){return []}

        // check if it is within limits
        if (new_score < 0){return}
        if (new_score > season.score_level_max){return}

        // check if its within teh dates
        let now = new Date().toISOString();

        if(now < season.start){return}

        let three_days = new Date(season.end_final);
        three_days.setDate(three_days.getDate() + 3);

        if(now > three_days.toISOString()){return}

        let tmp:Season_User = {
            id: `${config.user}_${season.season}_${new_score}`,

            user: config.user,
            season: season.season,

            date_updated: new Date().toISOString(),
            level: new_score,

        }

        // add new or remove old
        if(new_score > last_score){
            // add
            await post_data("POST", `${backend}/season/user/${season.season}`, tmp);
            user_stats.push(tmp)
        }else{
            // remove
            await post_data("DELETE", `${backend}/season/user/${season.season}`, tmp);
            user_stats = user_stats.filter(stat => stat.level !== new_score)
        }

        this.setState({user_stats})

    }

    create_data_estimate = (): Create_Data_Estimate => {
        let { user_stats, season, score, config } = this.state;
        let date_start = new Date().toISOString();
        let score_level = 1;
        // get the start date
        for (let i = 0; i < user_stats.length; i++) {
            // get current score level
            if (user_stats[i].level > score_level) {
                score_level = user_stats[i].level;
            }
            // get the current start date
            if (user_stats[i].date_updated < date_start) {
                date_start = user_stats[i].date_updated;
            }
        }

        // intercept it here
        if(config.date_start !== ""){
            date_start = config.date_start;
        }

        // days between the
        let days_elapsed =  ((new Date().getTime() - new Date(date_start).getTime()) / (1000 * 3600 * 24) + 1)


        // get teh xp/day since that date
        let xp_gained: number;
        let xp_per_day: number;
        let percentage_complete: number;
        if (score_level === 1) {
            xp_gained = 0;
            xp_per_day = 0;
            percentage_complete = 0;
        } else {
            let {total_xp, percentage_of_total} = score[score_level - 1];
            xp_gained = total_xp;
            xp_per_day = xp_gained/days_elapsed;
            percentage_complete = percentage_of_total
        }


        // go from now to the end, generating the graph data


        let days_total = ((new Date(season.end_final).getTime() - new Date(season.start).getTime())/(1000 * 3600 * 24) + 2)
        let days_remaining = ((new Date(season.end_final).getTime() - new Date().getTime()) / (1000 * 3600 * 24) + 1);

        let graph_data: Graph_Data[] = [];
        let score_level_max = score_level;
        let counter = 0;
        // set teh default to days until the event ends
        let days_to_max = days_remaining;
        for(let i=1;i< (days_total);i++){
            // date, used a lot here
            let date = new Date(season.start);
            date.setDate(date.getDate() + (i-1));

            // if the date is less than now pay no attention to it
            if(date.getTime() < new Date().getTime()){continue}
            counter += 1;

            let date_string = date.toISOString();

            let xp_for_day = xp_gained + (xp_per_day * counter);

            let level = 0;
            for(let j=(score_level-1);j<season.score_level_max;j++){
                level = j+1;

                if (xp_for_day < score[j].total_xp){
                    break
                }
            }
            // days to end or 100
            if(level <= season.score_level_board){
                if (score_level_max < season.score_level_board){
                    days_to_max = counter;
                }

            }
            // grab the max score for the user
            if (level > score_level_max){
                score_level_max = level;
            }


            graph_data.push(
                {
                    group: "User - Estimate",
                    date: date_string,
                    value: level
                }
            )
        }
        if (score_level <= 1){
            graph_data = [];
            score_level_max = 1;
        }

        return {
            score_starting: score_level,
            estimate_graph: graph_data,
            estimate_final_level: score_level_max,
            // days to reach that level
            estimate_final_level_days: days_to_max,
            days_remaining: Math.round(days_remaining),
            date_start: date_start,
            percentage_complete: percentage_complete
        }
    }

    override_date_start = () =>{
        let {season, config, backend} = this.state;

        let date_min = new Date(season.start);
        date_min.setDate(date_min.getDate()-1);

        let date_max = season.end_final;
        let now = new Date().toISOString();
        if(now < date_max){
            date_max = now;
        }

        let date_start: string | undefined = undefined;
        if(config.date_start !== ""){
            date_start = config.date_start;
        }

        return <DatePicker
            value={date_start}
            minDate={date_min}
            maxDate={date_max}
            //dateFormat="d/m/Y"
            datePickerType="single"
            short={true}
            onClose={
                async (e: Date[])=>{
                    if(e.length >0){
                        config.date_start = e[0].toISOString();
                    } else{
                        config.date_start = "";
                    }
                    this.setState({config: config})

                    // update it in teh db
                    await post_data("POST", `${backend}/season/config/${season.season}`, config)
                }
            }
        >
            <DatePickerInput
                id={`date-picker-calendar-id-${season.season}`}
                //placeholder="dd/mm/yyyy"
                labelText="Override Start Date (clear to reset)"
                type="text"
                size={"sm"}
            />
        </DatePicker>
    }

    create_days_data = (): Create_Days_Data =>{
        let { season, show_all, score } = this.state;
        const start_date = new Date(season.start);

        // +2 to include the start and end days
        let days = ((new Date(season.end_final).getTime() - start_date.getTime())/(1000 * 3600 * 24) + 2)

        let xp_max = score[season.score_level_board].total_xp;
        let xp_day = xp_max/days;

        let row_data: Days_Data[]= [];
        let graph_target: Graph_Data[] = [];

        for(let i=1;i< (days);i++){
            let level = 0;
            let xp_day_today = xp_day * i;

            for(let j=1;j<season.score_level_board;j++){
                level = j;

                if (xp_day_today < score[j].total_xp){
                    break
                }
            }

            let date = new Date(season.start);
            date.setDate(date.getDate() + (i-1));
            let date_string = date.toISOString();

            graph_target.push(
                {
                    group: "Target",
                    date: date_string,
                    value: level +1
                }
            )


            let date2 = new Date(season.start);
            date2.setDate(date2.getDate() + (i));
            // depends if these are being shown or not
            if (
                !show_all &&
                date2.getTime() < new Date().getTime()
            ){continue}

            //console.log(selected)
            row_data.push({
                id: i.toString(),
                day: i,
                // to whatever the viewer wants to see
                date: date_string.substring(0, 10),
                level: level +1,
                days_left: days - i - 1
            })
        }


        let days_original = ((new Date(season.end).getTime() - start_date.getTime())/(1000 * 3600 * 24) + 2)

        let xp_day_original = xp_max/days_original;

        for(let i=1;i< (days_original);i++){
            let level = 0;
            let xp_day_today = xp_day_original * i;

            for(let j=1;j<season.score_level_board;j++){
                level = j;

                if (xp_day_today < score[j].total_xp){
                    break
                }
            }

            let date = new Date(season.start);
            date.setDate(date.getDate() + (i-1));
            let date_string = date.toISOString();


            graph_target.push(
                {
                    group: "Target_Original",
                    date: date_string,
                    value: level +1
                }
            )
        }

        return {days_data: row_data,graph_target: graph_target }
    }

    create_user_data = (): { graph_user: Graph_Data[] } =>{
        let {user_stats} = this.state;
        let graph_user: Graph_Data[] = [];
        for(let i=0;i<user_stats.length;i++){
            let { level, date_updated } = user_stats[i]

            graph_user.push({
                date: date_updated,
                group: "User",
                value: level
            })
        }
        return {graph_user: graph_user }
    }

    create_data_challenges = (last_score: number):Create_Data_Challenges =>{
        let {season, score} = this.state;

        const start_date = new Date(season.start);

        // +2 to include the start and end days
        let days = ((new Date(season.end_final).getTime() - start_date.getTime())/(1000 * 3600 * 24) + 1)

        let graph_data: Graph_Data[] = [];

        // xp earned
        let xp_day_to_today = {
            pve: 0,
            pvx: 0
        };
        let days_to_complete_user = {
            pve: 0,
            pvx: 0
        }
        let days_to_complete = {
            pve: 0,
            pvx: 0
        }
        let first_mult = 1;

        // // use the score required to get to this level
        let xp_user = 0

        if(last_score > 1){
            xp_user = score[last_score-1].total_xp
        }

        let challenge_max_user = {
            pve: last_score,
            pvx: last_score
        }

        let xp_day_to_today_user = {
            pve: xp_user,
            pvx: xp_user
        };

        let counter = 0;
        for(let i=1;i< (days);i++){
            // date, used a lot here
            let date = new Date(season.start);
            date.setDate(date.getDate() + (i-1));
            let date_string = date.toISOString();

            if(date.getTime() >= new Date().getTime()){
                counter += 1;
            }

            let pve = season.score_daily;
            let pvx = season.score_daily + season.score_daily_nw

            // season 8 had no Nuclear Winter
            if(season.season === 8){
                // seperate count of the pvx
                pvx = season.score_daily;
            }

            // add the weeklies on as well
            if(date.getDay() === 2){
                // week rolled over, add the weeklies
                pve += season.score_weekly;
                pvx += season.score_weekly;
            }else {
                if(i===1){
                    // just in case the week dosent start with tuesday
                    pve += season.score_weekly;
                    pvx += season.score_weekly;
                }
            }

            if(season.season >= 9){
                pvx = pvx * first_mult;
            }

            xp_day_to_today.pve += pve;
            xp_day_to_today.pvx += pvx;

            if(counter > 0){
                xp_day_to_today_user.pve += pve;
                xp_day_to_today_user.pvx += pvx;
            }

            let level_PvX = 0;
            for(let j=1;j<season.score_level_max;j++){
                level_PvX = j +1;

                if (xp_day_to_today.pvx < score[j].total_xp){
                    break
                }
            }
            // user
            let level_PvX_user = 0;
            for(let j=1;j<season.score_level_max;j++){
                level_PvX_user = j +1;

                if (xp_day_to_today_user.pvx < score[j].total_xp){
                    break
                }
            }

            let level_PvE = 0;
            // cycle through calculated score levels
            for(let j=1;j<season.score_level_max;j++){
                level_PvE = j +1;

                if (xp_day_to_today.pve < score[j].total_xp){
                    break
                }
            }

            let level_PvE_user = 0;
            for(let j=1;j<season.score_level_max;j++){
                level_PvE_user = j +1;

                if (xp_day_to_today_user.pve < score[j].total_xp){
                    break
                }
            }

            if(level_PvX <= season.score_level_board){
                days_to_complete_user.pvx = counter;
                days_to_complete.pvx = i;
            }
            if(level_PvX_user <= season.score_level_max){
                if(level_PvX_user > challenge_max_user.pvx){
                    challenge_max_user.pvx = level_PvX_user;
                }
            }

            if(level_PvE <= season.score_level_board){
                days_to_complete_user.pve = counter;
                days_to_complete.pve = i;
            }

            if(level_PvE_user > challenge_max_user.pve){
                challenge_max_user.pve = level_PvE_user;
            }
            if(level_PvE_user <= season.score_level_max){
                if(level_PvE_user > challenge_max_user.pve){
                    challenge_max_user.pve = level_PvE_user;
                }
            }

            let group_pvx = "PvX";
            let group_pvx_user = "User - PvX";

            let group_pve = "PvE";
            let group_pve_user = "User - PvE";

            let add_user = counter > 0 && last_score > 1;

            if(season.season >= 9){
                group_pvx = "1st";
                group_pvx_user = "User - 1st";

                // handle the multiplication
                if(level_PvX > 15){
                    first_mult = 1.05
                }
                if(level_PvX > 50){
                    first_mult = 1.15
                }
                if(level_PvX > 75){
                    first_mult = 1.25
                }
            }

            graph_data.push(
              {
                  group: group_pvx,
                  date: date_string,
                  value: level_PvX
              }
            )
            graph_data.push(
              {
                  group: group_pve,
                  date: date_string,
                  value: level_PvE
              }
            )
            if(add_user){
                graph_data.push(
                  {
                      group: group_pvx_user,
                      date: date_string,
                      value: level_PvX_user
                  }
                )
                graph_data.push(
                  {
                      group: group_pve_user,
                      date: date_string,
                      value: level_PvE_user
                  }
                )
            }
        }

        return {
            days_to_complete_user: days_to_complete_user,
            days_to_complete: days_to_complete,
            estimate_final_score: challenge_max_user,
            challenge_graph: graph_data
        }
    }

    create_chart = (graph_target: Graph_Data[],graph_user: Graph_Data[], challenge_graph: Graph_Data[], estimate_graph: Graph_Data[] ) =>{
        let merged: Graph_Data[] = [];

        merged.push(...graph_target);
        merged.push(...graph_user);
        merged.push(...challenge_graph);
        merged.push(...estimate_graph);

        if(merged.length === 0){return null}


        let options: LineChartOptions = {
            title: "Score over time",
            data: {
                selectedGroups: ["Target"]
            },
            axes: {
                bottom: {
                    title: "Time",
                    mapsTo: "date",
                    scaleType: ScaleTypes.TIME
                },
                left: {
                  mapsTo: "value",
                  title: "Level",
                  scaleType: ScaleTypes.LINEAR,
                  thresholds: [
                    {
                      value: 100,
                      label: "Finish",
                      fillColor: "orange"
                    }
                  ]
                }
            },
            curve: "curveMonotoneX",
            points: {
                radius: 2
            },
            timeScale: {
                addSpaceOnEdges: 0
            },
            zoomBar: {
                top: {
                    enabled: true
                }
            },
            height: "400px"
        }

        // Dispite being defined above TS needs it double checked
        if(options.data && options.data.selectedGroups){

            if(graph_user.length > 0){
                options.data.selectedGroups.push("User")

                if(estimate_graph.length > 0){
                    options.data.selectedGroups.push("User - Estimate")
                }
            }else{
                options.data.selectedGroups.push("PvE")
            }
        }

        return <ChartManager data={merged} options={options} />
    }

    // this is about using repeatable challenges
    // has a number input box so its a seperate component with its own state
    create_table_repeatable_challenges = (last_score: number, estimate_final_score: PvX_PvE, estimate_data: Create_Data_Estimate) =>{
        let {season, score, pvx_name} = this.state;
        return <RepeatableChallengesTable score={score} season={season} last_score={last_score} estimate_final_score={estimate_final_score} estimate_history={estimate_data.estimate_final_level} pvx_name={pvx_name} />
    }

    // information for the user
    create_table_score_user = (data_challenges: Create_Data_Challenges, estimate_data: Create_Data_Estimate) =>{
        let {season, pvx_name} = this.state;
        let {days_to_complete_user, days_to_complete, estimate_final_score} = data_challenges;

        let season_length_original = ((new Date(season.end).getTime() - new Date(season.start).getTime())/(1000 * 3600 * 24) + 1)
        let season_length_extended = ((new Date(season.end_final).getTime() - new Date(season.start).getTime())/(1000 * 3600 * 24) + 1)

        let days_pve = days_to_complete.pve >= 0 ? days_to_complete.pve.toFixed(0) : 0;
        let days_pvx = days_to_complete.pvx >= 0 ? days_to_complete.pvx.toFixed(0) : 0;

        let days_pve_user = days_to_complete_user.pve >= 0 ? days_to_complete_user.pve.toFixed(0) : 0;
        let days_pvx_user = days_to_complete_user.pvx >= 0 ? days_to_complete_user.pvx.toFixed(0) : 0;
        let days_estimate_user = estimate_data.estimate_final_level_days >= 0 ? estimate_data.estimate_final_level_days.toFixed(0) : 0;

        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell head noWrap>Estimates</StructuredListCell>
                    <StructuredListCell head noWrap>PvE</StructuredListCell>
                    <StructuredListCell head noWrap>{pvx_name}</StructuredListCell>
                    <StructuredListCell head noWrap>Estimate</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Final Level</StructuredListCell>
                    <StructuredListCell>{estimate_final_score.pve}</StructuredListCell>
                    <StructuredListCell>{estimate_final_score.pvx}</StructuredListCell>
                    <StructuredListCell>{estimate_data.estimate_final_level}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Your Days</StructuredListCell>
                    <StructuredListCell>{days_pve_user}</StructuredListCell>
                    <StructuredListCell>{days_pvx_user}</StructuredListCell>
                    <StructuredListCell>{days_estimate_user}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Total Days</StructuredListCell>
                    <StructuredListCell>{days_pve}</StructuredListCell>
                    <StructuredListCell>{days_pvx}</StructuredListCell>
                    <StructuredListCell/>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell/>
                    <StructuredListCell/>
                    <StructuredListCell/>
                    <StructuredListCell/>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell/>
                    <StructuredListCell>Original</StructuredListCell>
                    <StructuredListCell>Extended</StructuredListCell>
                    <StructuredListCell>Remaining</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Days</StructuredListCell>
                    <StructuredListCell>{season_length_original}</StructuredListCell>
                    <StructuredListCell>{season_length_extended}</StructuredListCell>
                    <StructuredListCell>{estimate_data.days_remaining}</StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    create_table_atoms = (last_score: number, estimate_final_score: PvX_PvE, estimate_data: Create_Data_Estimate) =>{
        let {season, pvx_name} = this.state;

        // add score cost to buy remaining lvls
        let levels_all = season.score_level_board - last_score;
        let levels_pve = season.score_level_board - estimate_final_score.pve;
        let levels_pvx = season.score_level_board - estimate_final_score.pvx;
        let levels_his = season.score_level_board - estimate_data.estimate_final_level;

        let atom_cost_all = season.atoms * levels_all;
        let atom_cost_pve = season.atoms * levels_pve;
        let atom_cost_pvx = season.atoms * levels_pvx;
        let atom_cost_his = season.atoms * levels_his;

        // EUR 0.0080/atom
        // GBP 0.0064/atom
        // USD 0.0080/atom
        // CAD 0.0100/atom
        // AUD 0.0120/atom

        let eur_ratio = 0.0080;
        let eur_prefix = "€"
        let atom_eur_all = `${eur_prefix}${(atom_cost_all * eur_ratio).toFixed(2)}`;
        let atom_eur_pve = `${eur_prefix}${(atom_cost_pve * eur_ratio).toFixed(2)}`;
        let atom_eur_pvx = `${eur_prefix}${(atom_cost_pvx * eur_ratio).toFixed(2)}`;
        let atom_eur_his = `${eur_prefix}${(atom_cost_his * eur_ratio).toFixed(2)}`;

        let gbp_ratio = 0.0064;
        let gbp_prefix = "£"
        let atom_gbp_all = `${gbp_prefix}${(atom_cost_all * gbp_ratio).toFixed(2)}`;
        let atom_gbp_pve = `${gbp_prefix}${(atom_cost_pve * gbp_ratio).toFixed(2)}`;
        let atom_gbp_pvx = `${gbp_prefix}${(atom_cost_pvx * gbp_ratio).toFixed(2)}`;
        let atom_gbp_his = `${gbp_prefix}${(atom_cost_his * gbp_ratio).toFixed(2)}`;

        let usd_ratio = 0.0080;
        let usd_prefix = "$"
        let atom_usd_all = `${usd_prefix}${(atom_cost_all * usd_ratio).toFixed(2)}`;
        let atom_usd_pve = `${usd_prefix}${(atom_cost_pve * usd_ratio).toFixed(2)}`;
        let atom_usd_pvx = `${usd_prefix}${(atom_cost_pvx * usd_ratio).toFixed(2)}`;
        let atom_usd_his = `${usd_prefix}${(atom_cost_his * usd_ratio).toFixed(2)}`;

        let cad_ratio = 0.0100;
        let cad_prefix = "$"
        let atom_cad_all = `${cad_prefix}${(atom_cost_all * cad_ratio).toFixed(2)}`;
        let atom_cad_pve = `${cad_prefix}${(atom_cost_pve * cad_ratio).toFixed(2)}`;
        let atom_cad_pvx = `${cad_prefix}${(atom_cost_pvx * cad_ratio).toFixed(2)}`;
        let atom_cad_his = `${cad_prefix}${(atom_cost_his * cad_ratio).toFixed(2)}`;

        let aud_ratio = 0.0120;
        let aud_prefix = "$"
        let atom_aud_all = `${aud_prefix}${(atom_cost_all * aud_ratio).toFixed(2)}`;
        let atom_aud_pve = `${aud_prefix}${(atom_cost_pve * aud_ratio).toFixed(2)}`;
        let atom_aud_pvx = `${aud_prefix}${(atom_cost_pvx * aud_ratio).toFixed(2)}`;
        let atom_aud_his = `${aud_prefix}${(atom_cost_his * aud_ratio).toFixed(2)}`;


        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell head noWrap>Atoms</StructuredListCell>
                    <StructuredListCell head noWrap>Season</StructuredListCell>
                    <StructuredListCell head noWrap>PvE</StructuredListCell>
                    <StructuredListCell head noWrap>{pvx_name}</StructuredListCell>
                    <StructuredListCell head noWrap>Estimate</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Levels</StructuredListCell>
                    <StructuredListCell>{levels_all}</StructuredListCell>
                    <StructuredListCell>{levels_pve}</StructuredListCell>
                    <StructuredListCell>{levels_pvx}</StructuredListCell>
                    <StructuredListCell>{levels_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Atoms</StructuredListCell>
                    <StructuredListCell>{atom_cost_all}</StructuredListCell>
                    <StructuredListCell>{atom_cost_pve}</StructuredListCell>
                    <StructuredListCell>{atom_cost_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_cost_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>EUR</StructuredListCell>
                    <StructuredListCell>{atom_eur_all}</StructuredListCell>
                    <StructuredListCell>{atom_eur_pve}</StructuredListCell>
                    <StructuredListCell>{atom_eur_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_eur_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>GBP</StructuredListCell>
                    <StructuredListCell>{atom_gbp_all}</StructuredListCell>
                    <StructuredListCell>{atom_gbp_pve}</StructuredListCell>
                    <StructuredListCell>{atom_gbp_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_gbp_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>USD</StructuredListCell>
                    <StructuredListCell>{atom_usd_all}</StructuredListCell>
                    <StructuredListCell>{atom_usd_pve}</StructuredListCell>
                    <StructuredListCell>{atom_usd_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_usd_his}</StructuredListCell>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>CAD</StructuredListCell>
                    <StructuredListCell>{atom_cad_all}</StructuredListCell>
                    <StructuredListCell>{atom_cad_pve}</StructuredListCell>
                    <StructuredListCell>{atom_cad_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_cad_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>AUD</StructuredListCell>
                    <StructuredListCell>{atom_aud_all}</StructuredListCell>
                    <StructuredListCell>{atom_aud_pve}</StructuredListCell>
                    <StructuredListCell>{atom_aud_pvx}</StructuredListCell>
                    <StructuredListCell>{atom_aud_his}</StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    create_table_rewards = (last_score: number): { to_get: any, got: any, total: any } =>{
        // this oputputs two tables, attained rewards and yet to attain rewards
        // spolit between normal and 1st

        let {rewards} = this.state;


        type temp_sub = {
            [propName in Season_Rewards_Type]: {
                [propName: string]: {
                    rarity: Season_Rewards_Rarity;
                    base: number;
                    first: number;
                }
            };
        };
        interface Tmp {
            [propName: string]: temp_sub;
        }

        let tmp: Tmp = {
            "To Get": {
                CAMP: {},
                Consumable: {},
                Currency: {},
                "Power Armor": {},
                Outfit: {},
                Flair: {},
                Frame: {},
                Icon: {},
                Pose: {},
                Emote: {},
                Skin: {},
                Stein: {},
                Ally: {},
            },
            "Received": {
                CAMP: {},
                Consumable: {},
                Currency: {},
                "Power Armor": {},
                Outfit: {},
                Flair: {},
                Frame: {},
                Icon: {},
                Pose: {},
                Emote: {},
                Skin: {},
                Stein: {},
                Ally: {},
            },
            "Total": {
                CAMP: {},
                Consumable: {},
                Currency: {},
                "Power Armor": {},
                Outfit: {},
                Flair: {},
                Frame: {},
                Icon: {},
                Pose: {},
                Emote: {},
                Skin: {},
                Stein: {},
                Ally: {},
            }
        };

        for (let i=0; i<rewards.length;i++){
            let reward = rewards[i];
            let group = "To Get";
            if(last_score >= reward.level){
                group = "Received";
            }

            if(typeof tmp[group][reward.type][reward.name] === "undefined"){
                tmp[group][reward.type][reward.name] = {
                    rarity: reward.rarity,
                    base: 0,
                    first: 0
                }
            }
            if(typeof tmp["Total"][reward.type][reward.name] === "undefined"){
                tmp["Total"][reward.type][reward.name] = {
                    rarity: reward.rarity,
                    base: 0,
                    first: 0
                }
            }


            if(reward.first){
                tmp[group][reward.type][reward.name].first += reward.quantity
                // always added to this
                tmp["Total"][reward.type][reward.name].first += reward.quantity
            }else{
                tmp[group][reward.type][reward.name].base += reward.quantity
                // always added to this
                tmp["Total"][reward.type][reward.name].base += reward.quantity
            }
        }


        let ordering_category:  Season_Rewards_Type[] = [
            Season_Rewards_Type.Currency,
            Season_Rewards_Type.Consumable,
            Season_Rewards_Type.PA,
            Season_Rewards_Type.Outfit,
            Season_Rewards_Type.CAMP,
            Season_Rewards_Type.Skin,
            Season_Rewards_Type.Flair,
            Season_Rewards_Type.Icon,
            Season_Rewards_Type.Pose,
            Season_Rewards_Type.Frame,
            Season_Rewards_Type.Stein,
            Season_Rewards_Type.Ally,
        ]
        // list rare items before not so rare
        let ordering_rarity: Season_Rewards_Rarity[] = [
            Season_Rewards_Rarity.Legendary,
            Season_Rewards_Rarity.Rare,
            Season_Rewards_Rarity.Common
        ];

        let process_tmp = (group: string) => {

            let data = tmp[group];

            let rows = [];

            let blank = true;
            for (let i=0;i<ordering_category.length;i++){
                let category = ordering_category[i];

                let data_category = data[category];

                let names = Object.keys(data_category).sort()
                if (names.length === 0){
                    // nothing in this category
                    continue
                }
                blank = false;

                let rows_sub = []

                // loop through rarities, legendary is first
                for(let j=0;j< ordering_rarity.length;j++){
                    let rarity = ordering_rarity[j];

                    for (let k=0;k<names.length;k++){
                        let name = names[k];
                        let item_details = data_category[name];
                        if (item_details.rarity !== rarity){continue}
                        rows_sub.push(
                            <StructuredListRow tabIndex={0}>
                                <StructuredListCell>{name}</StructuredListCell>
                                <StructuredListCell>{item_details.rarity}</StructuredListCell>
                                <StructuredListCell>{item_details.base}</StructuredListCell>
                                <StructuredListCell>{item_details.first}</StructuredListCell>
                            </StructuredListRow>
                        )

                    }
                }

                rows.push(<AccordionItem title={category}>
                    <StructuredListWrapper ariaLabel="Structured list" >
                        <StructuredListBody>
                            <StructuredListRow tabIndex={0}>
                                <StructuredListCell head noWrap>Item</StructuredListCell>
                                <StructuredListCell head noWrap>Rarity</StructuredListCell>
                                <StructuredListCell head noWrap>Base</StructuredListCell>
                                <StructuredListCell head noWrap>1st</StructuredListCell>
                            </StructuredListRow>
                            {rows_sub}
                        </StructuredListBody>
                    </StructuredListWrapper>
                </AccordionItem>)
            }

            if(blank){return null}

            return <div className="bx--col">
                <h5>Rewards: {group}</h5>
                <br />
                <Accordion align='end'  style={{minWidth: 350, maxWidth: 500}}>
                    {rows}
                </Accordion>
            </div>
        }

        return {
            to_get: process_tmp("To Get"),
            got: process_tmp("Received"),
            total: process_tmp("Total"),
        }
    }

    create_explain_table = () =>{
        let {pvx_name} = this.state;
        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListHead>
                <StructuredListRow head tabIndex={0}>
                    <StructuredListCell head>
                        Term
                    </StructuredListCell>
                    <StructuredListCell head>
                        Explanation
                    </StructuredListCell>
                </StructuredListRow>
            </StructuredListHead>
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        Target
                    </StructuredListCell>
                    <StructuredListCell>
                        This is the target Score Level for the day, based off of when the season ends (or if it has been extended).
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        Target Original
                    </StructuredListCell>
                    <StructuredListCell>
                        This is like Target but with the original end date.
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        User
                    </StructuredListCell>
                    <StructuredListCell>
                        This is a record of your score over time (assuming you increment it above).
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        PvE
                    </StructuredListCell>
                    <StructuredListCell>
                        These are the non Nuclear Winter (NW) Score challenges
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        {pvx_name}
                    </StructuredListCell>
                    <StructuredListCell>
                        {pvx_name} are the PvE challenges + NW
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        Estimate
                    </StructuredListCell>
                    <StructuredListCell>
                        This is based on how fast you have gotten to your current Score Level
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        Repeatable
                    </StructuredListCell>
                    <StructuredListCell>
                        Shows information on the repeatable challenge. Season is if you only did it to 100
                    </StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>
                        Atoms
                    </StructuredListCell>
                    <StructuredListCell>
                        Shows information on buying levels. Season is if you only did it to 100
                    </StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    create_scrolling_box = (data: any, height: string = "500px") =>{
        return <div style={{width: "100%", height: height, overflow: "hidden"}}>
            <div
                style={
                    {
                        width: "100%",
                        height: "100%",
                        overflowY: "scroll",
                        paddingRight: "17px", /* Increase/decrease this value for cross-browser compatibility */
                        boxSizing: "content-box" /* So the width will be 100% + 17px */
                    }
                }
            >
                {data}
            </div>
        </div>
    }

    render() {
        let { season, show_all, score, savant, backend, config } = this.state;

        if(Object.keys(score).length === 0){return null}

        let data_estimate = this.create_data_estimate();
        let {score_starting, estimate_graph,estimate_final_level} = data_estimate;

        let {days_data, graph_target} = this.create_days_data();
        let {graph_user} = this.create_user_data();

        let data_challenges = this.create_data_challenges(score_starting);
        let { challenge_graph, estimate_final_score } = data_challenges;

        let rewards_tables = this.create_table_rewards(score_starting);

        let row_1_col_1
        let row_2 = null;
        let row_3 = null;
        if(savant){
            row_1_col_1 = <div className="bx--col"  style={{maxWidth: 350, minWidth: 350}}>
                <SavantTable data_estimate={data_estimate} estimate_final_score={estimate_final_score} season={season} />
            </div>

        }else{
            row_1_col_1 = <div className="bx--col" style={{maxWidth: 350, minWidth: 350}}>
                <div className="bx--row">
                    <div className="bx--col">
                        <h5>Tracker</h5>
                    </div>
                    <div className="bx--col" >
                        <Toggle
                            toggled={show_all}
                            onToggle={()=>{this.setState({show_all: !show_all})}}
                            labelText={"Show all"}
                            id={`Toggle-season-${season.season}`}
                        />
                    </div>
                </div>
                {this.create_scrolling_box(this.create_days_table(days_data), "450px")}
            </div>

            row_2 = <div className="bx--row">
                <div className="bx--col">
                    {this.create_table_score_user(data_challenges, data_estimate)}
                </div>
                <div className="bx--col">
                    {this.create_table_repeatable_challenges(score_starting, estimate_final_score, data_estimate)}
                </div>
                <div className="bx--col">
                    <LevelTable score={score} season={season} backend={backend} config={config} />
                </div>
                <div className="bx--col">
                    {this.create_table_atoms(score_starting, estimate_final_score, data_estimate)}
                </div>
                { /* these return teh div with teh col class if there is any data in tehm */ }
                {rewards_tables.got}
                {rewards_tables.to_get}
                {rewards_tables.total}
            </div>

            row_3 = <div className="bx--row">
                <div className="bx--col" style={{maxWidth: 360}}>
                    <h5>Xp Required</h5>
                    {this.create_scrolling_box(this.create_score_table())}
                </div>
                <div className="bx--col">
                    <h5>Explain terms</h5>
                    {this.create_explain_table()}
                </div>
            </div>
        }

        let row_0_col_3 = null;
        if (estimate_final_level > 0){
            row_0_col_3 = <div className="bx--col">
                {this.override_date_start()}
            </div>
        }

        return <div id={`Tab-sub-${season.season}_sub`}>
            <div className="bx--row">
                <div className="bx--col" style={{maxWidth: 350, minWidth: 350}}>
                    <h3>{season.name}</h3>
                </div>
                <div className="bx--col" style={{maxWidth: 225}}>
                    {this.update_score(score_starting)}
                </div>
                {row_0_col_3}
            </div>
            <br />
            <div className="bx--row">
                {row_1_col_1}
                <div className="bx--col">
                    <h5>Graph</h5>
                    {this.create_chart(graph_target, graph_user, challenge_graph, estimate_graph)}
                </div>
            </div>
            <br />
            {row_2}

            <br />
            {row_3}
        </div>
    }
}

interface Chart_Props {
    data: any[];
    options: LineChartOptions;
}
// what it uses
interface Chart_State {
    data: any[];
    options: LineChartOptions;
}
class ChartManager extends Component <Chart_Props, Chart_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            data: this.props.data,
            options: this.props.options,
        }
    }

    componentDidUpdate = async(prevProps: Chart_Props) => {
        // if user is now set
        if(prevProps.data.length !== this.props.data.length){
            this.setState({data: this.props.data, options:this.props.options })
        }else{
            // check the contents
            let isSame = prevProps.data.every((o,i) => Object.keys(o).length === Object.keys(this.props.data[i]).length && Object.keys(o).every(k => o[k] === this.props.data[i][k]));

            if(!isSame){
                this.setState({data: this.props.data, options:this.props.options })
            }
        }
    }

    render() {
        let {data, options} = this.state;

        // @ts-ignore
        const Chart = ({ data, options }) => (
            <LineChart data={data} options={options} />
        );

        // @ts-ignore
        return <Chart data={data} options={options} />;
    }
}

interface RepeatableChallengesTable_Props {
    score: {[propName: number]: Score_XP};
    season: Season;

    last_score: number;
    estimate_final_score: PvX_PvE;
    estimate_history: number;
    pvx_name: string;
}
// what it uses
interface RepeatableChallengesTable_State {
    mob_box: number;
    score: {[propName: number]: Score_XP};
    season: Season;

    last_score: number;
    estimate_final_score: PvX_PvE;
    estimate_history: number;
    pvx_name: string;
}
class RepeatableChallengesTable extends Component <RepeatableChallengesTable_Props, RepeatableChallengesTable_State>{
    constructor(props: any) {
        super(props);
        this.state = {
            // starts out at 500, remains internal
            mob_box: 500,

            // these dont get updated
            score: this.props.score,
            season: this.props.season,

            // these get updated
            last_score: this.props.last_score || 1,
            estimate_final_score: this.props.estimate_final_score || {pve: 0, pvx:0},
            estimate_history: this.props.estimate_history || 1,
            pvx_name: this.props.pvx_name
        }
    }

    componentDidUpdate = async(prevProps: RepeatableChallengesTable_Props) => {
        // if user is now set

        if(prevProps.last_score !== this.props.last_score){
            this.setState({last_score: this.props.last_score})
        }
        if(prevProps.estimate_history !== this.props.estimate_history){
            this.setState({estimate_history: this.props.estimate_history})
        }
        if(
            prevProps.estimate_final_score.pve !== this.props.estimate_final_score.pve ||
            prevProps.estimate_final_score.pvx !== this.props.estimate_final_score.pvx
        ){
            this.setState({estimate_final_score: this.props.estimate_final_score})
        }


    }

    create_misc_stats_table = () =>{
        let { mob_box, score, season, last_score, estimate_final_score, estimate_history, pvx_name } = this.state;

        // create this first to get it out of the way
        let mob_box_input = <input
            type={"number"}
            id={`mob-box2-${season.season}`}
            min={0}
            //max={100}
            step={50}
            value={mob_box}
            onChange={(e) => {
                this.setState({mob_box:parseInt(e.target.value, 10)})
            }}
            style={{maxWidth: 60}}
        />

        let max_pve = Math.min(estimate_final_score.pve, 100)
        let max_pvx = Math.min(estimate_final_score.pvx, 100)
        let max_est = Math.min(estimate_history, 100)


        let levels_season = 100 - last_score;
        let levels_pve = 100 - max_pve;
        let levels_pvx = 100 - max_pvx;
        let levels_his = 100 - max_est;

        let score_xp_season = last_score > 1 ? score[last_score].total_xp : 0;
        let score_xp_pve = max_pve > 1 ? score[max_pve].total_xp : 0
        let score_xp_pvx = max_pvx > 1 ? score[max_pvx].total_xp : 0
        let score_xp_his = max_est > 1 ? score[max_est].total_xp : 0

        let score_xp_required_season = score[100].total_xp - score_xp_season;
        let score_xp_required_pve = score[100].total_xp - score_xp_pve;
        let score_xp_required_pvx = score[100].total_xp - score_xp_pvx;
        let score_xp_required_his = score[100].total_xp - score_xp_his;

        let challenges_required_season = Math.round(score_xp_required_season/100);
        let challenges_required_pve = Math.round(score_xp_required_pve/100);
        let challenges_required_pvx = Math.round(score_xp_required_pvx/100);
        let challenges_required_his = Math.round(score_xp_required_his/100);

        let mobs_to_finish_season = Math.round((score_xp_required_season * season.ratio)/mob_box);
        let mobs_to_finish_pve = Math.round((score_xp_required_pve * season.ratio)/mob_box);
        let mobs_to_finish_pvx = Math.round((score_xp_required_pvx * season.ratio)/mob_box);
        let mobs_to_finish_his = Math.round((score_xp_required_his * season.ratio)/mob_box);

        let days = Math.max((new Date(season.end_final).getTime() - new Date().getTime())/(1000 * 3600 * 24), 1)


        // challenges per day
        let challenges_per_day_season = Math.round(challenges_required_season/days)
        let challenges_per_day_pve = Math.round(challenges_required_pve/days)
        let challenges_per_day_pvx = Math.round(challenges_required_pvx/days)
        let challenges_per_day_his = Math.round(challenges_required_his/days)

        // mobs per day

        let mobs_per_day_season = Math.round(mobs_to_finish_season/days)
        let mobs_per_day_pve = Math.round(mobs_to_finish_pve/days)
        let mobs_per_day_pvx = Math.round(mobs_to_finish_pvx/days)
        let mobs_per_day_his = Math.round(mobs_to_finish_his/days)


        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell head noWrap>Repeatable</StructuredListCell>
                    <StructuredListCell head noWrap>Season</StructuredListCell>
                    <StructuredListCell head noWrap>PvE</StructuredListCell>
                    <StructuredListCell head noWrap>{pvx_name}</StructuredListCell>
                    <StructuredListCell head noWrap>Estimate</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Levels to 100</StructuredListCell>
                    <StructuredListCell>{levels_season}</StructuredListCell>
                    <StructuredListCell>{levels_pve}</StructuredListCell>
                    <StructuredListCell>{levels_pvx}</StructuredListCell>
                    <StructuredListCell>{levels_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Challenges to 100</StructuredListCell>
                    <StructuredListCell>{challenges_required_season}</StructuredListCell>
                    <StructuredListCell>{challenges_required_pve}</StructuredListCell>
                    <StructuredListCell>{challenges_required_pvx}</StructuredListCell>
                    <StructuredListCell>{challenges_required_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Challenges Per Day</StructuredListCell>
                    <StructuredListCell>{challenges_per_day_season}</StructuredListCell>
                    <StructuredListCell>{challenges_per_day_pve}</StructuredListCell>
                    <StructuredListCell>{challenges_per_day_pvx}</StructuredListCell>
                    <StructuredListCell>{challenges_per_day_his}</StructuredListCell>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>XP Per Mob</StructuredListCell>
                    <StructuredListCell>{mob_box_input}</StructuredListCell>
                    <StructuredListCell/>
                    <StructuredListCell/>
                    <StructuredListCell/>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Mobs To kill</StructuredListCell>
                    <StructuredListCell>{mobs_to_finish_season}</StructuredListCell>
                    <StructuredListCell>{mobs_to_finish_pve}</StructuredListCell>
                    <StructuredListCell>{mobs_to_finish_pvx}</StructuredListCell>
                    <StructuredListCell>{mobs_to_finish_his}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Mobs Per Day</StructuredListCell>
                    <StructuredListCell>{mobs_per_day_season}</StructuredListCell>
                    <StructuredListCell>{mobs_per_day_pve}</StructuredListCell>
                    <StructuredListCell>{mobs_per_day_pvx}</StructuredListCell>
                    <StructuredListCell>{mobs_per_day_his}</StructuredListCell>
                </StructuredListRow>

            </StructuredListBody>
        </StructuredListWrapper>

    }

    render() {
        return this.create_misc_stats_table()
    }
}


interface SavantTable_Props {
    data_estimate: Create_Data_Estimate;
    estimate_final_score: PvX_PvE;
    season: Season;
}
interface SavantTable_State {
    data_estimate: Create_Data_Estimate;
    estimate_final_score: PvX_PvE;
    nuclear_winter: boolean;
    season: Season;
}
class SavantTable extends Component <SavantTable_Props, SavantTable_State>{
    constructor(props: SavantTable_Props) {
        super(props);
        this.state = {
            // not updated
            season: this.props.season,

            // updated
            data_estimate: this.props.data_estimate,
            estimate_final_score: this.props.estimate_final_score,

            // controlled by conponent
            nuclear_winter: false,

        }
    }

    componentDidUpdate = async(prevProps: SavantTable_Props) => {
        if(
            prevProps.data_estimate.score_starting !== this.props.data_estimate.score_starting ||
            prevProps.data_estimate.date_start !== this.props.data_estimate.date_start ||
            prevProps.data_estimate.estimate_graph.length !== this.props.data_estimate.estimate_graph.length ||
            prevProps.data_estimate.estimate_final_level !== this.props.data_estimate.estimate_final_level ||
            prevProps.data_estimate.estimate_final_level_days !== this.props.data_estimate.estimate_final_level_days ||
            prevProps.data_estimate.percentage_complete !== this.props.data_estimate.percentage_complete ||
            prevProps.data_estimate.days_remaining !== this.props.data_estimate.days_remaining
        ){
            this.setState({data_estimate: this.props.data_estimate})
        }

        if(
            prevProps.estimate_final_score.pve !== this.props.estimate_final_score.pve ||
            prevProps.estimate_final_score.pvx !== this.props.estimate_final_score.pvx
        ){
            this.setState({estimate_final_score: this.props.estimate_final_score})
        }
    }

    create_misc_stats_table = () =>{
        let {nuclear_winter, season, data_estimate, estimate_final_score} = this.state;

        // split this out for readability
        let {score_starting, estimate_final_level, days_remaining, date_start, percentage_complete } = data_estimate;

        // pvx stuff
        let text = "Do you do Nuclear Winter?";
        let disabled = false;

        if(season.season === 8){
            disabled = true;
        }

        if(season.season >= 9){
            text = "Do you have 1st?";
        }

        let toggle = <Toggle
            toggled={nuclear_winter}
            disabled={disabled}
            onToggle={()=>{this.setState({nuclear_winter: !nuclear_winter})}}
            labelText={""}
            id={`Toggle-savant-${season.season}`}
            labelA={"No"}
            labelB={"Yes"}
        />

        let days_elapsed =  (((new Date().getTime() - new Date(date_start).getTime()) / (1000 * 3600 * 24))).toFixed(2)

        let text_final_row;
        if(estimate_final_level > 0){
            text_final_row = `Final score based on how fast you have gotten to Level ${score_starting} since ${days_elapsed} days ago. (When you started using this tool)`
        }else{
            text_final_row = `You have not yet inputted your Score Rank`
        }

        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Percentage complete</StructuredListCell>
                    <StructuredListCell>{percentage_complete}%</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Days left</StructuredListCell>
                    <StructuredListCell>{days_remaining}</StructuredListCell>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>{text}</StructuredListCell>
                    <StructuredListCell>{toggle}</StructuredListCell>
                </StructuredListRow>

                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Score from Challenges</StructuredListCell>
                    <StructuredListCell>{nuclear_winter ? estimate_final_score.pvx : estimate_final_score.pve}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>{text_final_row}</StructuredListCell>
                    <StructuredListCell>{estimate_final_level}</StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    render() {
        return this.create_misc_stats_table()
    }
}


interface LevelTable_Props {
    score: Score_Object;
    season: Season;
    backend: string;
    config: Season_Config;
}
interface LevelTable_State {
    score: Score_Object;
    season: Season;
    backend: string;
    config: Season_Config;
    start: number;
    end: number;
}
class LevelTable extends Component <LevelTable_Props, LevelTable_State>{
    constructor(props: LevelTable_Props) {
        super(props);
        this.state = {
            // not updated
            score: this.props.score,
            season: this.props.season,

            backend: this.props.backend,
            config: this.props.config,

            // updated
            start: this.props.config.level_start || 1 ,
            end: this.props.config.level_end || 1,
        }
    }

    componentDidMount = async()=>{

    }

    create_table = () =>{
        let {score, season , end, start, backend, config} = this.state;

        let xp_gained = 0;
        for(let i=start; i<end;i++){
            if(i<1000){
                xp_gained += (160*(i-1)) + 200;
            }else{
                xp_gained += 159880;
            }
        }

        let xp_score = 0
        if(xp_gained > 0){
            xp_score = Math.round(xp_gained/season.ratio)
        }

        let levels_0 = 0;
        let levels_100 = 0;

        let xp_from_max = 0;
        // starting from 100 and going down
        for(let i=season.score_level_board;i>0;i--){
            let {total_xp, level, to_next} = score[i];


            if(xp_score < total_xp){
                levels_0 = level;
            }

            if(xp_score < xp_from_max){
                if(levels_100 === 0){
                    levels_100 = season.score_level_board - level;
                }
            }else{
                xp_from_max += to_next;
            }
        }

        let start_input = <input
            type={"number"}
            id={`level-box-start-${season.season}`}
            min={1}
            value={start}
            onChange={async(e) => {
                let level = parseInt(e.target.value, 10);

                config.level_start = level;

                await post_data("POST", `${backend}/season/config/${season.season}`, config)

                this.setState({start:level, config: config})
            }}
            style={{maxWidth: 60}}
        />;

        let end_input = <input
            type={"number"}
            id={`level-box-end-${season.season}`}
            min={1}
            value={end}
            onChange={async (e) => {
                let level = parseInt(e.target.value, 10);

                config.level_end = level;

                await post_data("POST", `${backend}/season/config/${season.season}`, config)

                this.setState({end:level, config: config})
            }}
            style={{maxWidth: 60}}
        />;


        return <StructuredListWrapper ariaLabel="Structured list">
            <StructuredListBody>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell head>Score from XP</StructuredListCell>
                    <StructuredListCell/>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Player Level Start</StructuredListCell>
                    <StructuredListCell>{start_input}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Player level End</StructuredListCell>
                    <StructuredListCell>{end_input}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Xp Gained</StructuredListCell>
                    <StructuredListCell>{xp_gained}</StructuredListCell>
                </StructuredListRow>


                <StructuredListRow tabIndex={0}>
                    <StructuredListCell head>Score</StructuredListCell>
                    <StructuredListCell/>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Score Xp Gained</StructuredListCell>
                    <StructuredListCell>{xp_score}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Levels from 1</StructuredListCell>
                    <StructuredListCell>{levels_0}</StructuredListCell>
                </StructuredListRow>
                <StructuredListRow tabIndex={0}>
                    <StructuredListCell>Levels back from {season.score_level_board}</StructuredListCell>
                    <StructuredListCell>{levels_100}</StructuredListCell>
                </StructuredListRow>
            </StructuredListBody>
        </StructuredListWrapper>
    }

    render() {
        return this.create_table()
    }
}